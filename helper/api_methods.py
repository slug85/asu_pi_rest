# coding=utf-8
__author__ = 'slugovskoy'
from sender.request_sender import Sender
import collections
from helper.validator import *
from helper.generator import *
import pytest
import allure
import collections
from conf import setup


class Rest:
    _obj = None
    _sender = None

    def __init__(self):
        self.sender = Sender()
        self.user = setup.user
        self.password = setup.password

    def __new__(cls, *dt):
        if cls._obj is None:
            cls._obj = object.__new__(cls, *dt)
        return cls._obj

    @pytest.allure.step('POST controllevels')
    def post_control(self, data):
        response = self.sender.post('controllevels', data)
        return response

    @pytest.allure.step('DELETE controllevels')
    def delete_control(self, control_id=None):
        response = self.sender.delete('controllevels/' + str(control_id))
        return response

    @pytest.allure.step('GET controllevels')
    def get_control_id(self, control_id=None):
        response = self.sender.get('controllevels/' + str(control_id))
        return response

    # maincontractors ==================================================
    @pytest.allure.step('POST maincontractors')
    def post_maincontractors(self, data):
        response = self.sender.post('maincontractors', data)
        return response

    @pytest.allure.step('PUT maincontractors')
    def put_maincontractors(self, data, id=None):
        response = self.sender.put('maincontractors/' + str(id), data)
        return response

    @pytest.allure.step('GET maincontractors')
    def get_maincontractors(self):
        response = self.sender.get('maincontractors')
        return response

    @pytest.allure.step('GET maincontractors with id {1}')
    def get_maincontractors_id(self, id=None):
        response = self.sender.get('maincontractors/' + str(id))
        return response

    @pytest.allure.step('DELETE maincontractors with id {1}')
    def delete_maincontractors_id(self, id=None):
        response = self.sender.delete('maincontractors/' + str(id))
        return response

    # /milestones ==================================================
    @pytest.allure.step('POST /milestones')
    def post_milestones(self, data):
        response = self.sender.post('milestones', data)
        return response

    @pytest.allure.step('PUT /milestones')
    def put_milestones(self, data, id=None):
        response = self.sender.put('milestones/' + str(id), data)
        return response

    @pytest.allure.step('GET /milestones')
    def get_milestones(self):
        response = self.sender.get('milestones')
        return response

    @pytest.allure.step('GET /milestones with id {1}')
    def get_milestones_id(self, id=None):
        response = self.sender.get('milestones/' + str(id))
        return response

    @pytest.allure.step('DELETE milestones with id {1}')
    def delete_milestones_id(self, id=None):
        response = self.sender.delete('milestones/' + str(id))
        return response

    # organizations ======================================================
    @pytest.allure.step('POST /organizations')
    def post_organizations(self, data):
        data['title'] = 'Организация'
        response = self.sender.post('organizations', data)
        return response

    @pytest.allure.step('PUT organizations')
    def put_organizations(self, data=None, id=None):
        response = self.sender.put('organizations/' + str(id), data)
        return response

    @pytest.allure.step('GET organizations with id {1}')
    def get_organizations_id(self, id=None):
        response = self.sender.get('organizations/' + str(id))
        return response
    
    @pytest.allure.step('POST /organizationmarks')
    def post_organizations_marks(self, data):
        data['title'] = 'Организация'
        response = self.sender.post('organizationmarks', data)
        return response

    @pytest.allure.step('PUT organizationmarks')
    def put_organizations_marks(self, data=None, id=None):
        response = self.sender.put('organizationmarks/' + str(id), data)
        return response

    @pytest.allure.step('GET organizationmarks with id {1}')
    def get_organizations_marks_id(self, id=None):
        response = self.sender.get('organizationmarks/' + str(id))
        return response

    @pytest.allure.step('GET organizationmarks')
    def get_organizations_marks(self):
        response = self.sender.get('organizationmarks')
        return response

    @pytest.allure.step('GET organizations parent with id {1}')
    def get_organizations_id_parent(self, id=None):
        response = self.sender.get('organizations/' + str(id) + '/parent')
        return response

    @pytest.allure.step('GET organizations persons with id {1}')
    def get_organizations_id_persons(self, id=None):
        response = self.sender.get('organizations/' + str(id) + '/persons')
        return response

    @pytest.allure.step('GET /organizations')
    def get_organizations(self):
        response = self.sender.get('organizations')
        return response

    @pytest.allure.step('GET /organizations/filter')
    def get_organizations_filter(self, filters=''):
        response = self.sender.get('organizations/filter?' + filters)
        return response

    @pytest.allure.step('GET /organizations/mainCustomer=true')
    def get_organizations_maincustomer(self):
        response = self.sender.get('organizations/mainCustomer=true')
        return response

    @pytest.allure.step('GET /organizations/?q=')
    def get_organizations_q(self, filters=''):
        response = self.sender.get('organizations?q=' + filters)
        return response

    @pytest.allure.step('GET /organizations?mainContractor=true')
    def get_organizations_maincontractor(self):
        response = self.sender.get('organizations?mainContractor=true')
        return response

    @pytest.allure.step('GET /organizations?externalControl=true ')
    def get_organizations_externalcontrol(self):
        response = self.sender.get('organizations?externalControl=true')
        return response

    @pytest.allure.step('GET /organizations?mainCustomer=true')
    def get_organizations_maincustomer(self):
        response = self.sender.get('organizations?mainCustomer=true')
        return response

    @pytest.allure.step('GET /organizations?mainExecutor=true')
    def get_organizations_mainexecutor(self):
        response = self.sender.get('organizations?mainExecutor=true')
        return response

    @pytest.allure.step('GET /organizations/roots')
    def get_organizations_roots(self):
        response = self.sender.get('organizations/roots')
        return response

    @pytest.allure.step('DELETE organizations with id {1}')
    def delete_organizations_id(self, id=None):
        response = self.sender.delete('organizations/' + str(id))
        return response

    @pytest.allure.step('DELETE organizations marks with id {1}')
    def delete_organizations_marks_id(self, id=None):
        response = self.sender.delete('organizationmarks/' + str(id))
        return response

    # projects ==========================================================
    @pytest.allure.step('GET /projects')
    def get_projects(self):
        response = self.sender.get('projects')
        return response

    @pytest.allure.step('GET /projects/roots')
    def get_projects_roots(self):
        response = self.sender.get('projects/roots')
        return response

    @pytest.allure.step('GET /projects/{1}')
    def get_projects_report(self, url):
        response = self.sender.get('projects/' + str(url))
        return response

    @pytest.allure.step('GET /projects/{1}/workPlan/report ')
    def get_projects_workplan_report(self, project_id):
        response = self.sender.get('projects/' + str(project_id) + '/workPlan/report')
        return response

    @pytest.allure.step('GET /projects/{1}/calendarPlan/report ')
    def get_projects_calendarplan_report(self, project_id):
        response = self.sender.get('projects/' + str(project_id) + '/calendarPlan/report')
        return response

    @pytest.allure.step('GET /projects/:id/reportStatusReports')
    def get_report_status_reports(self, project_id):
        response = self.sender.get('projects/' + str(project_id) + '/reportStatusReports')
        return response

    @pytest.allure.step('GET /projects/:id/arrangementPlans/:planId/report')
    def get_arrangement_plans_report(self, project_id=None, plan_id=None):
        response = self.sender.get('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id) + '/report')
        return response

    # financialsources ==================================================
    @pytest.allure.step('PUT financialsources')
    def put_financial_source(self, data=None, id=None):
        response = self.sender.put('financialsources/' + str(id), data)
        return response

    @pytest.allure.step('GET financialsources')
    def get_financialsources(self):
        response = self.sender.get('financialsources')
        return response

    @pytest.allure.step('GET financialsources with id {1}')
    def get_financialsources_id(self, id=None):
        response = self.sender.get('financialsources/' + str(id))
        return response

    @pytest.allure.step('DELETE financialsources with id {1}')
    def delete_financialsources_id(self, id=None):
        response = self.sender.delete('financialsources/' + str(id))
        return response

    @pytest.allure.step('POST financialsources')
    def post_financial_source(self, data):
        response = self.sender.post('financialsources', data)
        return response

    @pytest.allure.step('POST budgetlines ')
    def post_budget_line(self, data):
        response = self.sender.post('budgetlines', data)
        return response

    # planpis ==================================================
    @pytest.allure.step('PUT planpis')
    def put_planpis(self, data=None, id=None):
        response = self.sender.put('planpis/' + str(id), data)
        return response

    @pytest.allure.step('GET planpis')
    def get_planpis(self):
        response = self.sender.get('planpis')
        return response

    @pytest.allure.step('GET planpis with id {1}')
    def get_planpis_id(self, id=None):
        response = self.sender.get('planpis/' + str(id))
        return response

    @pytest.allure.step('GET planpis versions with id {1}')
    def get_planpis_versions_id(self, id=None):
        response = self.sender.get('planpiversions/' + str(id))
        return response

    @pytest.allure.step('GET planpis versions last')
    def get_planpis_versions_last(self):
        response = self.sender.get('planpiversions/last')
        return response

    @pytest.allure.step('GET planpis versions last')
    def get_planpis_summary_report(self, plan_id=1):
        response = self.sender.get('reports/planpiworkssummary/' + str(plan_id))
        return response

    @pytest.allure.step('DELETE planpis with id {1}')
    def delete_planpis_id(self, id=None):
        response = self.sender.delete('planpis/' + str(id))
        return response

    @pytest.allure.step('POST planpis')
    def post_planpis(self, data):
        response = self.sender.post('planpis', data)
        return response
    
    # project_tag =============================
    @pytest.allure.step('PUT projecttags')
    def put_projecttags(self, data=None, id=None):
        response = self.sender.put('projecttags/' + str(id), data)
        return response

    @pytest.allure.step('GET projecttags')
    def get_projecttags(self):
        response = self.sender.get('projecttags')
        return response

    @pytest.allure.step('GET projecttags with id {1}')
    def get_projecttags_id(self, id=None):
        response = self.sender.get('projecttags/' + str(id))
        return response

    @pytest.allure.step('DELETE projecttags with id {1}')
    def delete_projecttags_id(self, id=None):
        response = self.sender.delete('projecttags/' + str(id))
        return response

    @pytest.allure.step('POST projecttags')
    def post_projecttags(self, data):
        response = self.sender.post('projecttags', data)
        return response
    
    # person_attribute =============================
    @pytest.allure.step('PUT personattributes')
    def put_personattributes(self, data=None, id=None):
        response = self.sender.put('personattributes/' + str(id), data)
        return response

    @pytest.allure.step('GET personattributes')
    def get_personattributes(self):
        response = self.sender.get('personattributes')
        return response

    @pytest.allure.step('GET personattributes with id {1}')
    def get_personattributes_id(self, id=None):
        response = self.sender.get('personattributes/' + str(id))
        return response

    @pytest.allure.step('DELETE personattributes with id {1}')
    def delete_personattributes_id(self, id=None):
        response = self.sender.delete('personattributes/' + str(id))
        return response

    @pytest.allure.step('POST personattributes')
    def post_personattributes(self, data):
        response = self.sender.post('personattributes', data)
        return response    

    # executor selection methods ========================================
    @pytest.allure.step('POST executorselectionmethods')
    def post_executor_selection(self, data):
        response = self.sender.post('executorselectionmethods', data)
        return response

    @pytest.allure.step('GET executorselectionmethods')
    def get_executor_selection(self):
        response = self.sender.get('executorselectionmethods')
        return response

    @pytest.allure.step('GET executorselectionmethods:id')
    def get_executor_selection_id(self, id):
        response = self.sender.get('executorselectionmethods/' + str(id))
        return response

    @pytest.allure.step('DELETE executorselectionmethods:id')
    def delete_executor_selection(self, id):
        response = self.sender.delete('executorselectionmethods/' + str(id))
        return response

    @pytest.allure.step('PUT executorselectionmethods:id')
    def put_executor_selection(self, id, data):
        response = self.sender.put('executorselectionmethods/' + str(id), data)
        return response

    # projectlifecyclestages ==================================================
    @pytest.allure.step('PUT projectlifecyclestages')
    def put_projectlifecyclestages(self, data=None, id=None):
        response = self.sender.put('projectlifecyclestages/' + str(id), data)
        return response

    @pytest.allure.step('GET projectlifecyclestages')
    def get_projectlifecyclestages(self):
        response = self.sender.get('projectlifecyclestages')
        return response

    @pytest.allure.step('GET projectlifecyclestages with id {1}')
    def get_projectlifecyclestages_id(self, id=None):
        response = self.sender.get('projectlifecyclestages/' + str(id))
        return response

    @pytest.allure.step('DELETE projectlifecyclestages with id {1}')
    def delete_projectlifecyclestages_id(self, id=None):
        response = self.sender.delete('projectlifecyclestages/' + str(id))
        return response

    @pytest.allure.step('POST projectlifecyclestages')
    def post_projectlifecyclestages(self, data):
        response = self.sender.post('projectlifecyclestages', data)
        return response

    # projectstates ==================================================
    @pytest.allure.step('PUT projectstates')
    def put_projectstates(self, data=None, id=None):
        response = self.sender.put('projectstates/' + str(id), data)
        return response

    @pytest.allure.step('GET projectstates')
    def get_projectstates(self):
        response = self.sender.get('projectstates')
        return response

    @pytest.allure.step('GET projectstates with id {1}')
    def get_projectstates_id(self, id=None):
        response = self.sender.get('projectstates/' + str(id))
        return response

    @pytest.allure.step('DELETE projectstates with id {1}')
    def delete_projectstates_id(self, id=None):
        response = self.sender.delete('projectstates/' + str(id))
        return response

    @pytest.allure.step('POST projectstates')
    def post_projectstates(self, data):
        response = self.sender.post('projectstates', data)
        return response

    # projectstatuses ==================================================
    @pytest.allure.step('PUT projectstatuses')
    def put_projectstatuses(self, data=None, id=None):
        response = self.sender.put('projectstatuses/' + str(id), data)
        return response

    @pytest.allure.step('GET projectstatuses')
    def get_projectstatuses(self):
        response = self.sender.get('projectstatuses')
        return response

    @pytest.allure.step('GET projectstatuses with id {1}')
    def get_projectstatuses_id(self, id=None):
        response = self.sender.get('projectstatuses/' + str(id))
        return response

    @pytest.allure.step('DELETE projectstatuses with id {1}')
    def delete_projectstatuses_id(self, id=None):
        response = self.sender.delete('projectstatuses/' + str(id))
        return response

    @pytest.allure.step('POST projectstatuses')
    def post_projectstatuses(self, data):
        response = self.sender.post('projectstatuses', data)
        return response
    
    # planpiworkstatuses ==================================================
    @pytest.allure.step('PUT planpiworkstatuses')
    def put_planpiworkstatuses(self, data=None, id=None):
        response = self.sender.put('planpiworkstatuses/' + str(id), data)
        return response

    @pytest.allure.step('GET planpiworkstatuses')
    def get_planpiworkstatuses(self):
        response = self.sender.get('planpiworkstatuses')
        return response

    @pytest.allure.step('GET planpiworkstatuses with id {1}')
    def get_planpiworkstatuses_id(self, id=None):
        response = self.sender.get('planpiworkstatuses/' + str(id))
        return response

    @pytest.allure.step('DELETE planpiworkstatuses with id {1}')
    def delete_planpiworkstatuses_id(self, id=None):
        response = self.sender.delete('planpiworkstatuses/' + str(id))
        return response

    @pytest.allure.step('POST planpiworkstatuses')
    def post_planpiworkstatuses(self, data):
        response = self.sender.post('planpiworkstatuses', data)
        return response
    
    # projecttypes ==================================================
    @pytest.allure.step('PUT projecttypes')
    def put_projecttypes(self, data=None, id=None):
        response = self.sender.put('projecttypes/' + str(id), data)
        return response

    @pytest.allure.step('GET projecttypes')
    def get_projecttypes(self):
        response = self.sender.get('projecttypes')
        return response

    @pytest.allure.step('GET projecttypes with id {1}')
    def get_projecttypes_id(self, id=None):
        response = self.sender.get('projecttypes/' + str(id))
        return response

    @pytest.allure.step('DELETE projecttypes with id {1}')
    def delete_projecttypes_id(self, id=None):
        response = self.sender.delete('projecttypes/' + str(id))
        return response

    @pytest.allure.step('POST projecttypes')
    def post_projecttypes(self, data):
        response = self.sender.post('projecttypes', data)
        return response
    
    # asu ==================================================
    @pytest.allure.step('PUT asu')
    def put_asu(self, data=None, id=None):
        response = self.sender.put('asu/' + str(id), data)
        return response

    @pytest.allure.step('GET asu')
    def get_asu(self):
        response = self.sender.get('asu')
        return response

    def get_asu_filter(self, q=''):
        response = self.sender.get('asu?' + 'q=' + q)
        return response

    @pytest.allure.step('GET asu with id {1}')
    def get_asu_id(self, id=None):
        response = self.sender.get('asu/' + str(id))
        return response

    @pytest.allure.step('DELETE asu with id {1}')
    def delete_asu_id(self, id=None):
        response = self.sender.delete('asu/' + str(id))
        return response

    @pytest.allure.step('POST asu')
    def post_asu(self, data):
        response = self.sender.post('asu', data)
        return response

    # projectfoundationtypes ==================================================
    @pytest.allure.step('PUT projectfoundationtypes')
    def put_projectfoundationtypes(self, data=None, id=None):
        response = self.sender.put('projectfoundationtypes/' + str(id), data)
        return response

    @pytest.allure.step('GET projectfoundationtypes')
    def get_projectfoundationtypes(self):
        response = self.sender.get('projectfoundationtypes')
        return response

    @pytest.allure.step('GET projectfoundationtypes with id {1}')
    def get_projectfoundationtypes_id(self, id=None):
        response = self.sender.get('projectfoundationtypes/' + str(id))
        return response

    @pytest.allure.step('DELETE projectfoundationtypes with id {1}')
    def delete_projectfoundationtypes_id(self, id=None):
        response = self.sender.delete('projectfoundationtypes/' + str(id))
        return response

    @pytest.allure.step('POST projectfoundationtypes')
    def post_projectfoundationtypes(self, data):
        response = self.sender.post('projectfoundationtypes', data)
        return response

    # subprograms ==================================================
    @pytest.allure.step('PUT subprograms')
    def put_subprograms(self, data=None, id=None):
        response = self.sender.put('subprograms/' + str(id), data)
        return response

    @pytest.allure.step('GET subprograms')
    def get_subprograms(self):
        response = self.sender.get('subprograms')
        return response
    @pytest.allure.step('GET get programs/:id/subprograms')
    def get_programs_id_subprograms(self, program_id):
        response = self.sender.get('programs/' + str(program_id) + '/subprograms')
        return response


    @pytest.allure.step('GET subprograms with id {1}')
    def get_subprograms_id(self, id=None):
        response = self.sender.get('subprograms/' + str(id))
        return response

    @pytest.allure.step('DELETE subprograms with id {1}')
    def delete_subprograms_id(self, id=None):
        response = self.sender.delete('subprograms/' + str(id))
        return response

    @pytest.allure.step('POST subprograms')
    def post_subprograms(self, data):
        response = self.sender.post('subprograms', data)
        return response

    @pytest.allure.step('POST prgram')
    def post_programs(self, data):
        response = self.sender.post('programs', data)
        return response

    @pytest.allure.step('PUT programs')
    def put_programs(self, data=None, id=None):
        response = self.sender.put('programs/' + str(id), data)
        return response

    @pytest.allure.step('DELETE programs with id {1}')
    def delete_programs_id(self, id=None):
        response = self.sender.delete('programs/' + str(id))
        return response

    # roles ==================================================
    @pytest.allure.step('PUT roles')
    def put_roles(self, data=None, id=None):
        response = self.sender.put('roles/' + str(id), data)
        return response

    @pytest.allure.step('GET roles')
    def get_roles(self):
        response = self.sender.get('roles')
        return response

    @pytest.allure.step('GET accesses')
    def get_accesses(self):
        response = self.sender.get('accesses')
        return response

    @pytest.allure.step('GET roles with id {1}')
    def get_roles_id(self, id=None):
        response = self.sender.get('roles/' + str(id))
        return response

    @pytest.allure.step('DELETE roles with id {1}')
    def delete_roles_id(self, id=None):
        response = self.sender.delete('roles/' + str(id))
        return response

    @pytest.allure.step('POST roles')
    def post_roles(self, data):
        response = self.sender.post('roles', data)
        return response

    # themes ==================================================
    @pytest.allure.step('PUT themes')
    def put_themes(self, data=None, id=None):
        response = self.sender.put('themes/' + str(id), data)
        return response

    @pytest.allure.step('GET themes')
    def get_themes(self):
        response = self.sender.get('themes')
        return response

    @pytest.allure.step('GET accesses')
    def get_accesses(self):
        response = self.sender.get('accesses')
        return response

    @pytest.allure.step('GET accessTypes')
    def get_access_types(self):
        response = self.sender.get('accessTypes')
        return response

    @pytest.allure.step('GET themes with id {1}')
    def get_themes_id(self, id=None):
        response = self.sender.get('themes/' + str(id))
        return response

    @pytest.allure.step('DELETE themes with id {1}')
    def delete_themes_id(self, id=None):
        response = self.sender.delete('themes/' + str(id))
        return response

    @pytest.allure.step('POST themes')
    def post_themes(self, data):
        response = self.sender.post('themes', data)
        return response

    @pytest.allure.step('GET random user')
    def get_random_user(self):
        return random.choice(self.get_users().json())

    # documents ==================================================

    @pytest.allure.step('PUT documents')
    def put_document(self, document):
        response = self.sender.put('documents/' + str(document.id), params=document.body)
        return response







    # statusreports ==================================================
    @pytest.allure.step('PUT statusreports')
    def put_statusreports(self, data=None, id=None):
        response = self.sender.put('statusreports/' + str(id), data)
        return response

    @pytest.allure.step('GET statusreports')
    def get_statusreports(self):
        response = self.sender.get('statusreports')
        return response

    @pytest.allure.step('GET statusreports with id {1}')
    def get_statusreports_id(self, id=None):
        response = self.sender.get('statusreports/' + str(id))
        return response


    @pytest.allure.step('POST statusreports')
    def post_statusreports(self, data):
        response = self.sender.post('statusreports', data)
        return response

    # projects ==================================================
    @pytest.allure.step('PUT projects')
    def put_projects(self, data=None, id=None):
        response = self.sender.put('projects/' + str(id), data)
        return response

    @pytest.allure.step('GET projects')
    def get_projects(self, mode=''):
        response = self.sender.get('projects' + '?mode=' + mode)
        return response

    @pytest.allure.step('GET projects filter')
    def get_projects_filter(self, q=''):
        response = self.sender.get('projects/filter?' + q)
        return response

    @pytest.allure.step('GET projects with id {1}')
    def get_projects_id(self, id=None, mode='short'):
        response = self.sender.get('projects/' + str(id) + '?mode=' + mode)
        return response

    @pytest.allure.step('GET projects calendarPlan with id {1}')
    def get_projects_id_calendar(self, id=None):
        response = self.sender.get('projects/' + str(id) + '/calendarPlan')
        return response

    @pytest.allure.step('GET projects arrangement plan with id {1}')
    def get_projects_id_arrangement_plan(self, id=None):
        response = self.sender.get('projects/' + str(id) + '/arrangementPlans')
        return response

    # /projects/:id/arrangementPlans/:planId/stagesBeforeDate/:date
    @pytest.allure.step('GET projects arrangement plan stage before date')
    def get_arrangement_plan_stage_before_date(self, project_id=None, plan_id=None, date=None):
        response = self.sender.get('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id) + "/stages/beforeDate/" + str(date))
        return response


    @pytest.allure.step('DELETE arrangement plan')
    def delete_projects_id_arrangement_plan(self, project_id=None, plan_id=None):
        response = self.sender.delete('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id))
        return response

    @pytest.allure.step('DELETE arrangement plan stage')
    def delete_projects_id_arrangement_plan_stage_id(self, project_id=None, plan_id=None, stage_id=None, confirm='', action=''):
        response = self.sender.delete('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id) + '/stages/' + str(stage_id) + confirm + action)
        return response

    @pytest.allure.step('POST projects arrangement plan')
    def post_projects_id_arrangement_plan(self, data, project_id=None):
        response = self.sender.post('projects/' + str(project_id) + '/arrangementPlans', data)
        return response

    @pytest.allure.step('PUT projects arrangement plan')
    def put_projects_id_arrangement_plan(self, data, project_id=None, plan_id=None):
        response = self.sender.put('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id), data)
        return response

    # arrangement plan stage ==========================
    @pytest.allure.step('POST /projects/:id/arrangementPlans/:planId/stages')
    def post_projects_id_arrangement_plan_id_stage(self, data, project_id=None, plan_id=None):
        response = self.sender.post('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id) + '/stages', data)
        return response

    @pytest.allure.step('PUT /projects/:id/arrangementPlans/:planId/stages/:stageId')
    def put_projects_id_arrangement_plan_id_stage(self, data, project_id=None, plan_id=None, stage_id=None):
        response = self.sender.put('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id) + '/stages/' + str(stage_id), data)
        return response

    @pytest.allure.step('GET /projects/:id/arrangementPlans/:planId/stages')
    def get_projects_id_arrangement_plan_id_stage(self, project_id=None, plan_id=None):
        response = self.sender.get('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id) + '/stages')
        return response

    @pytest.allure.step('GET /projects/:id/arrangementPlans/:planId/stages/:stageId')
    def get_projects_id_arrangement_plan_id_stage_id(self, project_id=None, plan_id=None, stage_id=None):
        response = self.sender.get('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id) + '/stages/' + str(stage_id))
        return response

    @pytest.allure.step('GET /projects/:id/arrangementPlans/:planId/statusReports')
    def get_projects_id_arrangement_plan_id_statusreport_id(self, project_id=None, plan_id=None, type=''):
        response = self.sender.get('projects/' + str(project_id) + '/arrangementPlans/' + str(plan_id) +'/statusReports' + type)
        return response

    @pytest.allure.step('GET projects workPlan')
    def get_projects_id_workPlan(self, project_id=None, filters=''):
        response = self.sender.get('projects/' + str(project_id) + '/workPlan' + filters)
        return response

    @pytest.allure.step('GET projects workPlan milestone before date')
    def get_projects_id_workPlan_milestone_before_date(self, project_id=None, date=None):
        response = self.sender.get('projects/' + str(project_id) + '/workPlan/milestonesBeforeDate/' + str(date))
        return response

    @pytest.allure.step('GET projects workPlan milestone for status report')
    def get_projects_id_workPlan_milestone_status_report(self, project_id=None):
        response = self.sender.get('projects/' + str(project_id) + '/workPlan/milestonesForStatusReport')
        return response

    @pytest.allure.step('PUT projects calendarPlan')
    def put_projects_calendarPlan(self, data=None, id=None):
        response = self.sender.put('projects/' + str(id) + '/calendarPlan', params=data)
        return response

    @pytest.allure.step('DELETE projects with id {1}')
    def delete_projects_id(self, id=None):
        response = self.sender.delete('projects/' + str(id))
        return response

    @pytest.allure.step('POST projects')
    def post_projects(self, data):
        response = self.sender.post('projects', data)
        return response

    # calendar plan ====================================================
    @pytest.allure.step('PUT calendarPlan')
    def put_calendarPlan(self, data, project_id=None):
        data['calendarPlanStages'] = [get_calendar_plan_stage()]
        response = self.sender.put('projects/' + str(project_id) + '/calendarPlan', data)
        return response

    # planpiworktypes ==================================================
    @pytest.allure.step('PUT planpiworktypes')
    def put_planpiworktypes(self, data=None, id=None):
        response = self.sender.put('planpiworktypes/' + str(id), data)
        return response

    @pytest.allure.step('GET planpiworktypes')
    def get_planpiworktypes(self):
        response = self.sender.get('planpiworktypes')
        return response

    @pytest.allure.step('GET planpiworktypes with id {1}')
    def get_planpiworktypes_id(self, id=None):
        response = self.sender.get('planpiworktypes/' + str(id))
        return response

    @pytest.allure.step('DELETE planpiworktypes with id {1}')
    def delete_planpiworktypes_id(self, id=None):
        response = self.sender.delete('planpiworktypes/' + str(id))
        return response

    @pytest.allure.step('POST planpiworktypes')
    def post_planpiworktypes(self, data):
        response = self.sender.post('planpiworktypes', data)
        return response

    @pytest.allure.step('POST planpi work')
    def post_planpi_work(self, data, project_id=1):
        response = self.sender.post('projects/' + str(project_id) + '/planPiWorks', data)
        return response

    @pytest.allure.step('post_planpiwork_mail_review')
    def post_planpiwork_mail_review(self, work_id=1, project_id=1):
        response = self.sender.post('projects/' + str(project_id) + '/planPiWorks/' + str(work_id) + '/mailReview')
        return response

    @pytest.allure.step('PUT planpi work')
    def put_planpi_work(self, data, work_id=None, project_id=1):
        response = self.sender.put('projects/' + str(project_id) + '/planPiWorks/' + str(work_id), data)
        return response

    @pytest.allure.step('GET related works')
    def get_related_works(self, work_id=None, project_id=1):
        response = self.sender.get('projects/' + str(project_id) + '/planPiWorks/' + str(work_id) + '/related')
        return response

    @pytest.allure.step('GET planpi work')
    def get_planpi_works(self, project_id):
        response = self.sender.get('projects/' + str(project_id) + '/planPiWorks')
        return response

    @pytest.allure.step('GET planpi work report')
    def get_planpi_work_report(self, project_id):
        response = self.sender.get('projects/' + str(project_id) + '/planPiWorks/report')
        return response

    # arrangements ==================================================
    @pytest.allure.step('PUT arrangements')
    def put_arrangements(self, data=None, id=None):
        response = self.sender.put('arrangements/' + str(id), data)
        return response

    @pytest.allure.step('GET arrangements')
    def get_arrangements(self):
        response = self.sender.get('arrangements')
        return response

    @pytest.allure.step('GET arrangements with id {1}')
    def get_arrangements_id(self, id=None):
        response = self.sender.get('arrangements/' + str(id))
        return response

    @pytest.allure.step('DELETE arrangements with id {1}')
    def delete_arrangements_id(self, id=None):
        response = self.sender.delete('arrangements/' + str(id))
        return response

    @pytest.allure.step('POST arrangements')
    def post_arrangements(self, data):
        response = self.sender.post('arrangements', data)
        return response

    # programs ==================================================
    @pytest.allure.step('GET programs')
    def get_programs(self):
        response = self.sender.get('programs')
        return response

    @pytest.allure.step('GET programs with id {1}')
    def get_programs_id(self, id=None):
        response = self.sender.get('programs/' + str(id))
        return response

    # AUTH ================================================================
    @pytest.allure.step('auth')
    def auth(self):
        response = self.sender.authorize()
        return response

    # users ==================================================
    @pytest.allure.step('PUT persons')
    def put_users(self, data=None, id=None):
        response = self.sender.put('persons/' + str(id), data)
        return response

    @pytest.allure.step('GET persons')
    def get_users(self, filter=''):
        response = self.sender.get('persons' + filter)
        return response

    @pytest.allure.step('GET persons')
    def get_persons_can_approve(self, doc_type=1):
        response = self.sender.get('persons?canApproveDocTypes=' + str(doc_type))
        return response

    @pytest.allure.step('GET persons with id {1}')
    def get_users_id(self, id=None):
        response = self.sender.get('persons/' + str(id))
        return response

    @pytest.allure.step('GET persons/id/projects with id {1}')
    def get_users_id_projects(self, id=None):
        response = self.sender.get('users/' + str(id) + '/projects')
        return response

    @pytest.allure.step('DELETE persons with id {1}')
    def delete_users_id(self, id=None):
        response = self.sender.delete('persons/' + str(id))
        return response

    @pytest.allure.step('POST persons')
    def post_users(self, data):
        response = self.sender.post('persons', data)
        return response
    
    # systemobjects ===============================================
    @pytest.allure.step('GET systemobjects')
    def get_systemobjects(self):
        response = self.sender.get('systemobjects')
        return response

    @pytest.allure.step('GET systemobjects with id {1}')
    def get_systemobjects_id(self, id=None):
        response = self.sender.get('systemobjects/' + str(id))
        return response

    @pytest.allure.step('POST statesettings')
    # state settings ===============================================
    def post_state_settings(self, route='milestone', data=None):
        response = self.sender.post('statesettings/' + route, data)
        return response

    def put_state_settings(self, route='milestone', id=None, data=None):
        response = self.sender.put('statesettings/' + route + '/' + id, data)
        return response

    @pytest.allure.step('DELETE state settings')
    def delete_state_settings(self, route='milestones', id=None):
        response = self.sender.delete('statesettings/' + route + '/' + str(id))
        return response

    @pytest.allure.step('GET state settings')
    def get_state_settings(self, route='milestones'):
        response = self.sender.get('statesettings/' + route)
        return response

    @pytest.allure.step('GET state settings')
    def get_state_settings_id(self, route='milestones', id=None):
        response = self.sender.get('statesettings/' + route + '/' + id)
        return response