# coding=utf-8
__author__ = 'slugovskoy'
from jsonschema import validate
import pytest
import allure
import json
import pprint


@pytest.allure.step('валидация json через jsonschema')
def validate_json(json_dump, schema):
    dump = json.dumps(json_dump, ensure_ascii=False)
    schema_dump = json.dumps(schema, ensure_ascii=False)
    allure.attach('json for validation', dump)
    allure.attach('schema for validation', schema_dump)
    print 'validate with json schema'
    validate(json_dump, schema)


@pytest.allure.step('проверка json на наличие данных')
def assert_that_json_has_data(response_json=None, data=None):
    allure.attach('json response', json.dumps(response_json).decode('unicode-escape').encode('utf8'))
    allure.attach('data', json.dumps(data).decode('unicode-escape').encode('utf8'))

    for key in data.keys():
        assert response_json[key] == data[key]

@pytest.allure.step('проверка на отсутствие key["value"] в массиве response_json')
def assert_key_not_in_json_array(response_json, key=None, value=None):
    allure.attach('json response', json.dumps(response_json).decode('unicode-escape').encode('utf8'))
    allure.attach('node:', str(key) + ' = ' + str(value))
    assert any(item[key] for item in response_json) != value

@pytest.allure.step('проверка на присутствие новых данных в массиве response_json')
def assert_key_in_json_array(response_json, key=None, value=None):
    allure.attach('json response', json.dumps(response_json).decode('unicode-escape').encode('utf8'))
    allure.attach('node:', str(key) + ' = ' + str(value))
    items = list()
    for item in response_json:
        items.append(item['name'])
    assert value.decode('utf-8') in items


@pytest.allure.step('проверка на присутствие нодов или нода в response_json')
def assert_node_in_json(response_json, keys=None, data=None):
    if isinstance(keys, list):
        for key in keys:
            value = data[key]
            allure.attach('json response', json.dumps(response_json).decode('unicode-escape').encode('utf8'))
            allure.attach('node:', str(key) + ' = ' + str(value))
            actual_value = response_json[key]
            assert actual_value == value

    if isinstance(keys, str):
        key = keys
        value = data[key]
        allure.attach('json response', json.dumps(response_json).decode('unicode-escape').encode('utf8'))
        allure.attach('node:', str(key) + ' = ' + str(value))
        actual_value = response_json[key]
        assert actual_value == value


@pytest.allure.step('проверка ответа апи отчета на корректность заголовков')
def check_report_response(response):
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/octet-stream'
    assert response.headers['Content-Length'] > 500


def check_all_object_fields(o, response):
    fields = [attr for attr in dir(o) if not callable(attr) and not attr.startswith("__")]
    for field in fields:
        if field != 'json':
            assert str(o.json()[field]) == str(response.json()[field])



