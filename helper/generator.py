# coding=utf-8
__author__ = 'slugovskoy'
import random
import string
import datetime
import time
import calendar
import datetime
import collections

def random_word(length=8):
    return ''.join(random.choice(string.lowercase) for i in range(length))

def random_control_level():
    levels = [1, 2, 3, 4, 5]
    return random.choice(levels)

def random_int(a):
    return random.randint(1, a)

def random_int_in_range(a, b):
    return random.randint(a, b)

def random_code():
    return random.randint(10000, 50000)

def random_name_with_date(length=8):
    word = ''.join(random.choice(string.lowercase) for i in range(length))
    now = datetime.datetime.now()
    date = now.strftime("%Y-%m-%d__%H-%M-%S")
    return word + ' ' + date

def random_boolean():
    return bool(random.getrandbits(1))

def random_email():
    email = random_word(10) + '@' + random_word(7) + '.org'
    return email

def get_unix_time():
    date = datetime.datetime.utcnow()
    return calendar.timegm(date.timetuple()) * 1000

def get_unix_time_in_future(days_number=10):
    days = random_int(days_number)
    future = datetime.datetime.utcnow() + datetime.timedelta(days=days)
    return calendar.timegm(future.timetuple()) * 1000

def get_unix_time_in_future_no_random(days_number=10):
    future = datetime.datetime.utcnow() + datetime.timedelta(days=days_number)
    return calendar.timegm(future.timetuple()) * 1000

def get_unix_time_in_past(days_number=10):
    future = datetime.datetime.utcnow() - datetime.timedelta(days=days_number)
    return calendar.timegm(future.timetuple()) * 1000

def get_calendar_plan_stage():
    # этап КП
    calendat_plan_stage = collections.OrderedDict()
    calendat_plan_stage['tuid'] = 1
    calendat_plan_stage['number'] = 1
    calendat_plan_stage['name'] = random_word(9)
    calendat_plan_stage['startDate'] = get_unix_time()
    calendat_plan_stage['endDate'] = get_unix_time_in_future()
    return calendat_plan_stage

def get_date_from_unixtime(time):
    date = datetime.datetime.fromtimestamp(time / 1000)
    return date



