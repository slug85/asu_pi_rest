установка пакетов
pip install -r requirements.txt

Запуск на дженкинсе:

добавить переменные окружения через плагин  https://wiki.jenkins-ci.org/display/JENKINS/EnvInject+Plugin
user=admin@progredis.ru
password=test
app_url=http://localhost:9000/

выгрузить код тестов https://slug85@bitbucket.org/slug85/asu_pi_rest.git
установить пакеты из файла requirements.txt на Python

установить на дженкинс плагин allure
настроить послесборочнные шаги:
 - Publish JUnit test result report: output/report.xml
 - Allure Report Generation: **/output версия 1.3.9

добавить шаг на запуск py.test tests -s --junitxml=${WORKSPACE}\output\report.xml --alluredir ${WORKSPACE}\output

