# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.project import *


class TestDates:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('ISSUE #2715 тестирование необязательнных дат ')
    @allure.story('POST     /projects')
    @pytest.mark.first
    def test_projects_post_dates(self):
        data = Project()

        response = self.rest.post_projects(data.json())
        response_json = response.json()
        project_id = response_json['id']

        response = self.rest.get_projects_id(project_id, mode='full')
        response_json = response.json()
        assert response_json['planEndTime'] is None
        assert response_json['planStartTime'] is None

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('ISSUE #2715 тестирование необязательнных дат ')
    @allure.story('PUT     /projectCalendarPlanStages ')
    def test_project_calendar_plan_stages_put_dates(self):

        data = dict()
        data['projectId'] = self.rest.get_projects().json()[-1]['id']
        calendarplan_stage = {"tuid": 1,
                              "number": 1,
                              "name": random_word(5),
                              "endDate": get_unix_time_in_future(),
                              "startDate": ""}
        data['calendarPlanStages'] = [calendarplan_stage]
        project_id = data['projectId']
        response = self.rest.put_calendarPlan(data, project_id=project_id)
        calendar_plan_id = response.json()['id']

        for item in response.json()['calendarPlanStages']:
            assert item['endDate'] != 0

        response = self.rest.get_projects_id_calendar(calendar_plan_id)
        for item in response.json()['calendarPlanStages']:
            assert item['endDate'] is not 0






