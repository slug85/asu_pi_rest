# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
from classes.subprogram import *
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.subprogram import *

class TestPrograms:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_programs()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/programs')
    @allure.story('GET     /programs')
    @pytest.mark.first
    def test_programs_get(self):

        assert self.response.status_code == 200
        TestPrograms.id = self.json_dump[0]['id']
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/programs')
    @allure.story('GET     /programs/:id')
    @pytest.mark.run(after='test_programs_get')
    def test_programs_get_id(self):

        response = self.rest.get_programs_id(TestPrograms.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/programs')
    @allure.story('POST     /programs')
    @pytest.mark.run(after='test_programs_get_id')
    def test_programs_post(self):

        program = Program()
        response = self.rest.post_programs(program.json())
        assert response.status_code == 200

        assert response.json()['name'] == program.name
        assert response.json()['shortName'] == program.shortName
        assert response.json()['description'] == program.description
        TestPrograms.new_program_id = response.json()['id']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/programs')
    @allure.story('PUT     /programs')
    @pytest.mark.run(after='test_programs_post')
    def test_programs_put(self):

        program = Program()
        response = self.rest.put_programs(program.json(), TestPrograms.new_program_id)
        assert response.status_code == 200

        assert response.json()['name'] == program.name
        assert response.json()['shortName'] == program.shortName
        assert response.json()['description'] == program.description

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/programs')
    @allure.story('DELETE     /programs')
    @pytest.mark.last
    def test_programs_delete(self):

        response = self.rest.delete_programs_id(TestPrograms.new_program_id)
        assert response.status_code == 200

        response = self.rest.get_programs()
        for item in response.json():
            assert item['id'] != TestPrograms.new_program_id
