# coding=utf-8
__author__ = 'slugovskoy'
from sender.request_sender import Sender
from helper.validator import *
from helper.generator import *
import pytest
import allure
import json
import os
import pprint
import collections
import time


class TestDocuments:
    @classmethod
    def setup_class(cls):
        cls.sender = Sender()
        cls.sender.authorize()
        cls.response = cls.sender.get('documents')
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.upload_file = open(cls.current_dir + "/upload_document.txt", 'r')
        TestDocuments.project_id = cls.sender.get('projects').json()[0]['id']
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_versions_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documents')
    @allure.story('POST     /documents ')
    def test_documents_post(self):
        # upload
        text = self.upload_file.read()
        response = self.sender.post_upload(text)

        assert response.status_code == 200

        # post
        data = collections.OrderedDict()
        data['name'] = random_name_with_date(5)
        data['numbered'] = False
        data['project'] = TestDocuments.project_id
        data['documentType'] = self.sender.get('documenttypes').json()[0]['id']
        data['description'] = random_word(10)
        data['documentStatus'] = self.sender.get('documentstatuses').json()[0]['id']
        data['approvedDate'] = 0

        response = self.sender.post('documents', data)

        TestDocuments.id = response.json()['id']
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documents')
    @allure.story('PUT     /documents/:id')
    @pytest.mark.run(after='test_documents_post')
    def test_documents_put(self):
        new_id = TestDocuments.id
        data = collections.OrderedDict()

        data['name'] = random_name_with_date(5)
        data['numbered'] = False
        data['project'] = TestDocuments.project_id
        data['documentType'] = self.sender.get('documenttypes').json()[0]['id']
        data['description'] = random_word(10)
        data['documentStatus'] = self.sender.get('documentstatuses').json()[0]['id']
        data['approvedDate'] = 0

        response = self.sender.put('documents/' + str(new_id), data)
        assert response.status_code == 200
        #TODO сделать прверку
        response_json = response.json()

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documents')
    @allure.story('POST    /documents/:id/approve')
    @pytest.mark.run(after='test_documents_put')
    def test_documents_post_approve(self):

        pytest.skip('переделать под утверждение с персоной')
        #pytest.skip('нет файлов на сервере')

        document_id = TestDocuments.id
        project_id = TestDocuments.project_id

        # этап КП
        calendat_plan_stage = collections.OrderedDict()
        calendat_plan_stage['tuid'] = 1
        calendat_plan_stage['number'] = 1
        calendat_plan_stage['name'] = random_word(9)
        calendat_plan_stage['startDate'] = get_unix_time()
        calendat_plan_stage['endDate'] = get_unix_time_in_future()

        # КП
        data_kp = dict()
        data_kp['projectId'] = project_id
        data_kp['calendarPlanStages'] = [calendat_plan_stage]
        response = self.sender.put('projects/' + str(project_id) + '/calendarPlan', data_kp)
        assert response.status_code == 200

        # Загрузка файла
        text = self.upload_file.read()
        response = self.sender.post_upload(text)
        assert response.status_code == 200

        uploaded_file_new = dict()

        # approved
        if len(self.sender.get('files').json()) == 0:
            pytest.skip('нет файлов на сервере')

        uploaded_file = self.sender.get('files').json()[-1]
        uploaded_file_new['bytes'] = uploaded_file['bytes']
        uploaded_file_new['id'] = uploaded_file['id']

        data = dict()
        data['approvedFiles'] = [uploaded_file_new]
        data['approvedPerson'] = {"id": 1}
        data['approvedPersonPosition'] = random_word(20)
        data['documentStatus'] = 4
        data['approvedDate'] = int(round(time.time()))

        response = self.sender.post('documents/' + str(document_id) + '/approve', data)
        assert response.status_code == 200

        # "approvable": true
        # "type": "approved"
        response = self.sender.get('documents/' + str(document_id))
        assert response.status_code == 200

        response_json = response.json()
        documentStatus = response_json['lastVersion']['documentStatus']['id']

        assert response_json['approved'] is True

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documents')
    @pytest.mark.run(after='test_documents_post_approve')
    @allure.story('GET     /documents/:id/versions')
    def test_documents_versions_get(self):

        document_id = TestDocuments.id
        response = self.sender.get('documents/' + str(document_id) + '/versions')
        assert response.status_code == 200
        response_json = response.json()
        TestDocuments.version_id = response_json[-1]['id']

        validate_json(response_json, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documents')
    @pytest.mark.run(after='test_documents_versions_get')
    @allure.story('GET     /documents/:id/versions/:versionId')
    def test_documents_versions_get_id(self):

        document_id = TestDocuments.id
        version_id = TestDocuments.version_id

        response = self.sender.get('documents/' + str(document_id) + '/versions/' + str(version_id))
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)



    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documents')
    @allure.story('DELETE  /documents/:id')
    @pytest.mark.last
    def test_documents_delete(self):

        document_id = TestDocuments.id

        response = self.sender.delete('documents/' + str(document_id))
        assert response.status_code == 200

        response = self.sender.get('documents')
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', document_id)



































