# coding=utf-8
from sender.request_sender import Sender
from helper.validator import *
from helper.generator import *
import pytest
import allure
import json
import os
import pprint
import collections


class TestDocumentgroups:
    @classmethod
    def setup_class(cls):

        cls.sender = Sender()
        cls.sender.authorize()
        cls.response = cls.sender.get('documentgroups')
        cls.json_dump = cls.response.json()

        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documentgroups')
    @allure.story('GET     /documentgroups ')
    def test_documentgroups_get(self):

        assert self.response.status_code == 200
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @allure.feature('/documentgroups')
    @allure.story('GET     /documentgroups/:id ')
    @pytest.mark.run(after='test_documentgroups_get')
    def test_documentgroups_get_id(self):

        documentgroup_id = self.json_dump[0]['id']
        response = self.sender.get('documentgroups/' + str(documentgroup_id))
        assert response.status_code == 200
        response_json = response.json()
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @allure.feature('/documentgroups')
    @allure.story('POST    /documentgroups')
    @pytest.mark.run(after='test_documentgroups_get_id')
    def test_documentgroups_post(self):

        data = collections.OrderedDict()
        data['name'] = random_word(7)
        data['description'] = random_word(17)
        data['shortName'] = random_word(5)
        data['systemObjects'] = []

        response = self.sender.post('documentgroups', data)
        response_json = response.json()
        assert response.status_code == 200
        assert_that_json_has_data(response_json, data)

        documentgroup_id = response_json['id']
        TestDocumentgroups.new_id = documentgroup_id

        response = self.sender.get('documentgroups/' + str(documentgroup_id))
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)
        assert_that_json_has_data(response_json, data)

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @allure.feature('/documentgroups')
    @allure.story('PUT     /documentgroups/:id')
    @pytest.mark.run(after='test_documentgroups_post')
    def test_documentgroups_put(self):
        docuentgroup_id = TestDocumentgroups.new_id

        data = collections.OrderedDict()
        data['name'] = random_word(7)
        data['description'] = random_word(17)
        data['shortName'] = random_word(5)
        data['systemObjects'] = []

        response = self.sender.put('documentgroups/' + str(docuentgroup_id), data)
        response_json = response.json()
        assert response.status_code == 200

        assert_that_json_has_data(response_json, data)
        validate_json(response_json, self.schema_id)

        response = self.sender.get('documentgroups/' + str(docuentgroup_id))
        response_json = response.json()
        assert response.status_code == 200

        assert_that_json_has_data(response_json, data)

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @allure.feature('/documentgroups')
    @allure.story('DELETE  /documentgroups/:id')
    @pytest.mark.run(after='test_documentgroups_put')
    def test_documentgroups_delete(self):

        docuentgroup_id = TestDocumentgroups.new_id
        response = self.sender.delete('documentgroups/' + str(docuentgroup_id))
        assert response.status_code == 200

        response = self.sender.get('documentgroups')
        response_json = response.json()
        assert response.status_code == 200

        assert_key_not_in_json_array(response_json, 'id', docuentgroup_id)











