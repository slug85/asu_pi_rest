# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from sender.request_sender import Sender
from sets import Set


class TestProjects:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.sender = Sender()
        cls.projects = cls.rest.get_projects(mode='full').json()

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/filter')
    def test_projects_filters_project_completion_year(self):

        # asu
        asu_max_id = self.rest.get_asu().json()[-1]['id']

        # organization
        self.organization = {"name": "fullName", "shortName": "", "telegraphName": "", "country": "", "city": "",
                             "postIndex": "", "address": "", "comments": "", "representativeName": "",
                             "representativePosition": "", "representativePhone": "", "representativeEmail": "",
                             "contacts": [], "description": "", 'name': random_name_with_date(10), 'mainExecutor': True,
                             'id': self.rest.get_organizations().json()[-1]['id']}

        # theme
        random_user_id = self.rest.get_random_user()['id']
        theme = dict()
        theme['coordinator'] = random_user_id
        theme['selectedCoordinatorId'] = random_user_id
        theme['number'] = random_int(2)
        theme['description'] = random_word(25)
        theme['name'] = random_word(7)
        theme['shortName'] = random_word(3)
        self.theme_id = self.rest.post_themes(theme).json()['id']
        self.theme_id2 = self.rest.post_themes(theme).json()['id']

        # organization
        self.rest.put_organizations(self.organization, str(self.organization['id']))

        # project
        project = dict()
        project['description'] = random_word(25)
        project['name'] = random_word(7)
        project['shortName'] = random_word(3)
        project['number'] = random_int(5)
        project['projectLifecycleStage'] = {"id": 1}
        project['projectState'] = {"id": 1}
        project['projectStatus'] = {"id": 1}
        project['projectType'] = {"id": random_int(3)}
        project['subprogram'] = {"id": random_int(7)}
        project['projectCoordinator'] = {"id": self.rest.get_users().json()[1]['id']}
        project['planStartTime'] = get_unix_time_in_future_no_random(365)
        project['planEndTime'] = get_unix_time_in_future_no_random(600)
        project['completionDate'] = get_unix_time_in_future_no_random(600)
        project['priceOhr'] = random_int(1000000)
        project['priceKv'] = random_int(1000000)
        project['mainExecutor'] = {"id": self.organization['id']}
        project['asus'] = [self.rest.get_asu_id(random_int(asu_max_id)).json(),
                           self.rest.get_asu_id(random_int(asu_max_id)).json(),
                           self.rest.get_asu_id(random_int(asu_max_id)).json()
        ]
        project['price'] = random_int_in_range(1000,100000)
        project['themes'] = [{"id": self.theme_id}, {"id": self.theme_id2}]
        self.rest.post_projects(data=project)

        # проверка projectCompletionYear в ответе фильра по всем проектам
        for project in self.projects:
                completion_year = project['plannedCompletionDate']
                if completion_year is not None:
                    response = self.rest.get_projects_filter(
                        q='projectCompletionYear=' + str(get_date_from_unixtime(completion_year).year))
                    assert response.status_code == 200
                    assert get_date_from_unixtime(project['plannedCompletionDate']).year == get_date_from_unixtime(
                        completion_year).year

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/filter')
    @pytest.mark.run(after='test_projects_filters_project_completion_year')
    def test_projects_filters_project_planned_year(self):

                planned_years = Set()
                # проверка года projectPlanYear в ответе фильра по всем проектам
                for project in self.projects:
                    planned_year = project['planStartTime']
                    if planned_year is not None:
                        planned_years.add(get_date_from_unixtime(planned_year).year)
                for y in planned_years:
                    response = self.rest.get_projects_filter(q='projectPlanYear=' + str(y))
                    assert response.status_code == 200
                    for pr in response.json():
                        assert get_date_from_unixtime(pr['planStartTime']).year == y

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/filter')
    @pytest.mark.run(after='test_projects_filters_project_planned_year')
    def test_projects_filters_project_project_type_id(self):
        # проверка projectTypeId в ответе фильра по всем проектам для уникальных значений projectTypeId
        project_type_ids = Set()
        for project in self.projects:
            project_type_id = project['projectType']['id']
            project_type_ids.add(project_type_id)

        for p_id in project_type_ids:
            response = self.rest.get_projects_filter(q='projectTypeId=' + str(p_id))
            for item in response.json():
                assert item['projectType']['id'] == p_id

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/filter')
    @pytest.mark.run(after='test_projects_filters_project_project_state_id')
    def test_projects_filters_project_lifecycle_stage_id(self):

        # проверка projectLifecycleStageId в ответе фильра по всем проектам для уникальных значений projectStateId
        project_life_cycle_stage_ids = Set()

        for project in self.projects:
            if project['projectLifecycleStage'] is not None:
                project_life_cycle_stage_id = project['projectLifecycleStage']['id']
                project_life_cycle_stage_ids.add(project_life_cycle_stage_id)

        for p_id in project_life_cycle_stage_ids:
            response = self.rest.get_projects_filter(q='projectLifecycleStageId=' + str(p_id))
            for item in response.json():
                assert item['projectLifecycleStage']['id'] == p_id

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/filter')
    @pytest.mark.run(after='test_projects_filters_project_project_state_id')
    def test_projects_filters_project_coordinator(self):

        # сет координаторов
        project_coordinators = Set()
        for project in self.projects:
            if project['projectCoordinator'] is not None:
                project_coordinators.add(project['projectCoordinator']['id'])

        for coordinator_id in project_coordinators:
            response = self.rest.get_projects_filter(q='projectCoordinators=' + str(coordinator_id))
            assert response.status_code == 200
            for project in response.json():
                assert project['projectCoordinator']['id'] == coordinator_id


    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/filter')
    @pytest.mark.run(after='test_projects_filters_project_coordinator')
    def test_projects_filters_asu_relations(self):

        project_asus = Set()
        for project in self.projects:
            if project['asus'] is not None:
                for item in project['asus']:
                    project_asus.add(item['id'])

        for asu in project_asus:
            response = self.rest.get_projects_filter(q='asuRelations=' + str(asu))
            assert response.status_code == 200
            for project in response.json():
                any(a['id'] for a in project['asus']) == asu

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/filter')
    @pytest.mark.run(after='test_projects_filters_project_project_state_id')
    def test_projects_filters_project_theme(self):
        project_themes = Set()
        for project in self.projects:
            if project['themes'] is not None:
                for t in project['themes']:
                    project_themes.add(t['id'])
        for theme_id in project_themes:
            response = self.rest.get_projects_filter(q='themes=' + str(theme_id))
            for project in response.json():
                any(a['id'] for a in project['themes']) == theme_id

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/filter')
    @pytest.mark.run(after='test_projects_filters_project_theme')
    def test_projects_filters_project_subprogram(self):
        project_subprograms = Set()
        for project in self.projects:
            if project['subprogram'] is not None:
                project_subprograms.add(project['subprogram']['id'])
        for subprogram_id in project_subprograms:
            response = self.rest.get_projects_filter(q='subprograms=' + str(subprogram_id))
            for project in response.json():
                assert project['subprogram']['id'] == subprogram_id

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/filter')
    @pytest.mark.run(after='test_projects_filters_project_theme')
    def test_projects_filters_project_control(self):

        project_controls = Set()
        for project in self.projects:
            if project['projectArrangementPlans'] is not None and len(project['projectArrangementPlans']) > 0:
                project_controls.add(project['projectArrangementPlans'][0]['controlLevel']['name'].encode('utf-8'))

        for control in project_controls:
            response = self.rest.get_projects_filter(q='control=' + str(control))
            for project in response.json():
                assert any(pm['controlLevel']['name'].encode('utf-8') == control for pm in project['projectArrangementPlans'])
