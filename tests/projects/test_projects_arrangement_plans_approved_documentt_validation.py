# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
from classes.document import Document
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.project import *
from classes.calendar_plan import *


class TestProjectsArrangementPlansValidation:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()

    # plan ------------------
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans  validation with approved document')
    @allure.story('POST /projects/:id/arrangementPlans')
    def test_projects_post_arrangement_plans_with_approved_doc(self):

        # post project
        project = Project()

        response = self.rest.post_projects(project.json())
        assert response.status_code == 200
        response_json = response.json()
        TestProjectsArrangementPlansValidation.project_id = response_json['id']
        TestProjectsArrangementPlansValidation.organization = self.rest.get_organizations().json()[-1]['id']

        # создание вехи
        milestone = Milestone(project_id=TestProjectsArrangementPlansValidation.project_id, confirm=True)
        response = self.rest.post_milestones(milestone.json())
        milestone_id = response.json()['id']
        assert response.status_code == 200

        # post plan
        arrangement_plan = ArrangementPlan(project_id=TestProjectsArrangementPlansValidation.project_id, control_date=get_unix_time_in_future(40))
        response = self.rest.post_projects_id_arrangement_plan(arrangement_plan.json(), project_id=TestProjectsArrangementPlansValidation.project_id)
        assert response.status_code == 200
        response_json = response.json()
        TestProjectsArrangementPlansValidation.pid = response_json['id']
        document_id = response_json['document']['id']
        document_name = response_json['document']['lastVersion']['name']
        pm_end_date = response_json['controlDate']

        # изменить план так чтобы он был корректен:
        # добавление этапа ПМ
        arrangement_plan_stage = ArrangementPlanStage(project_id=TestProjectsArrangementPlansValidation.project_id, arrangement_plan=TestProjectsArrangementPlansValidation.pid, end_date=pm_end_date, number=1)
        response = self.rest.post_projects_id_arrangement_plan_id_stage(data=arrangement_plan_stage.json(), project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid)
        assert response.status_code == 200
        stage_id = response.json()['id']

        # добавление мероприятия в этапе
        arrangement = Arrangement(arrangement_plan_stage=stage_id, end_date=pm_end_date, arrangement_plan=TestProjectsArrangementPlansValidation.pid, number=1)
        response = self.rest.post_arrangements(arrangement.json())
        arrangement_id = response.json()['id']
        assert response.status_code == 200

        # связь мероприятия с вехой
        arrangement.milestones = [{"id": milestone_id}]
        response = self.rest.put_arrangements(data=arrangement.json(), id=arrangement_id)
        assert response.status_code == 200

        TestProjectsArrangementPlansValidation.document = Document(id=document_id, document_status=4, project=TestProjectsArrangementPlansValidation.project_id, document_type=6, name=document_name, approved=True)
        TestProjectsArrangementPlansValidation.document_not_approved = Document(id=document_id, document_status=1, project=TestProjectsArrangementPlansValidation.project_id, document_type=6, name=document_name)
        # утвердить документ ПМа
        response = self.rest.put_document(TestProjectsArrangementPlansValidation.document)
        assert response.status_code == 200

    # post stage
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans  validation with approved document')
    @allure.story('POST /projects/:id/arrangementPlans/:planId/stages')
    @pytest.mark.run(after='test_projects_post_arrangement_plans_with_approved_doc')
    def test_projects_post_arrangement_plans_stage_with_approved_doc(self):

        data = dict()
        data['endDate'] = get_unix_time_in_future(1)
        data['name'] = random_word(5)
        data['number'] = random_int_in_range(99, 199)
        data['projectArrangementPlan'] = TestProjectsArrangementPlansValidation.pid
        response = self.rest.post_projects_id_arrangement_plan_id_stage(data, project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid)
        warning = unicode('После редактирования плана мероприятий будет необходимо загрузить в систему утверждённый документ ПМ', 'utf-8')
        assert response.status_code == 400
        assert response.json()['type'] == 'toBeConfirmed'
        assert response.json()['message'] == warning

        self.rest.put_document(TestProjectsArrangementPlansValidation.document_not_approved)
        data = dict()
        data['endDate'] = get_unix_time_in_future(1)
        data['name'] = random_word(5)
        data['number'] = random_int_in_range(33, 88)
        data['projectArrangementPlan'] = TestProjectsArrangementPlansValidation.pid
        response = self.rest.post_projects_id_arrangement_plan_id_stage(data, project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid)
        assert response.status_code == 200
        TestProjectsArrangementPlansValidation.stage_id = response.json()['id']
        response = self.rest.get_projects_id_arrangement_plan_id_stage(project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid)
        assert response.status_code == 200
        assert_node_in_json(response.json(), data)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans  validation with approved document')
    @allure.story('DELETE /projects/:id/arrangementPlans/:planId/stages/:stageId')
    @pytest.mark.run(after='test_projects_post_arrangement_plans_stage_with_approved_doc')
    def test_projects_delete_arrangement_plans_stage_action_move_with_approved_doc(self):

        self.rest.put_document(TestProjectsArrangementPlansValidation.document)
        response = self.rest.delete_projects_id_arrangement_plan_stage_id(project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid, stage_id=TestProjectsArrangementPlansValidation.stage_id)
        assert response.status_code == 200
        self.rest.get_projects_id_arrangement_plan_id_stage(project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid)













































