# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from sender.request_sender import Sender
from classes.calendar_plan import *
from classes.project import *


class TestProjects:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())
        cls.schema_calendar = json.loads(open(cls.current_dir + "/schema_calendar_plan.json", 'r').read())
        cls.schema_work_plan = json.loads(open(cls.current_dir + "/schema_work_plan.json", 'r').read())
        cls.schema_roots = json.loads(open(cls.current_dir + "/schema_roots.json", 'r').read())
        cls.schema_work_plan_milestone = json.loads(
            open(cls.current_dir + "/schema_calendar_plan_milestone.json", 'r').read())
        cls.sender = Sender()

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('POST     /projects')
    @pytest.mark.first
    def test_projects_post(self):
        pytest.skip('включать понеобходимости')
        for i in range(10):

            # 10 этапов
            stages = list()
            for j in range(10):
                stage1 = CalendarPlanStage()
                stages.append(stage1)

            calendar_plan = CalendarPlan(calendar_plan_stages=stages)

            # проект
            project = Project()
            response = self.rest.post_projects(project.json())
            pid = response.json()['id']

            self.rest.put_projects_calendarPlan(data=calendar_plan.json(), id=pid)

            # 20 вех
            for n in range(20):
                milestone = Milestone(project_id=pid, confirm=True)
                self.rest.post_milestones(milestone.json())
























