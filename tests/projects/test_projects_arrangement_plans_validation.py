# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
from classes.document import Document
import pytest
import allure
import json
import os
import pprint
import collections
import time


class TestProjectsArrangementPlansValidation:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()

    # plan ------------------
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans validation')
    @allure.story('POST /projects/:id/arrangementPlans')
    def test_projects_post_arrangement_plans(self):

        # post project
        data = dict()
        data['description'] = random_word(25)
        data['name'] = random_word(7)
        data['shortName'] = random_word(3)
        data['number'] = random_int(5)
        data['projectLifecycleStage'] = {"id": 1}
        data['projectState'] = {"id": 1}
        data['projectStatus'] = {"id": 1}
        data['projectType'] = {"id": 1}
        data['subprogram'] = {"id": 1}
        data['projectCoordinator'] = {"id": self.rest.get_users().json()[0]['id']}

        response = self.rest.post_projects(data)
        assert response.status_code == 200
        response_json = response.json()
        TestProjectsArrangementPlansValidation.project_id = response_json['id']
        TestProjectsArrangementPlansValidation.organization = self.rest.get_organizations().json()[-1]['id']
        TestProjectsArrangementPlansValidation.main_executor = self.rest.get_organizations().json()[-1]

        # post plan
        controlOrganization = dict()
        data = dict()
        data['projectId'] = TestProjectsArrangementPlansValidation.project_id
        data['name'] = random_word(random_int(10))
        data['controlDate'] = get_unix_time_in_future(40)
        data['controlLevel'] = 2
        data['description'] = random_name_with_date()
        data['files'] = []

        controlOrganization['controlOrganization'] = {"id": TestProjectsArrangementPlansValidation.organization}
        controlOrganization['controlDate'] = get_unix_time_in_future(40)
        controlOrganization['description'] = random_name_with_date()
        controlOrganization['files'] = []

        data['controlOrganizations'] = [controlOrganization]
        data['arrangementPlanControls'] = [controlOrganization]

        response = self.rest.post_projects_id_arrangement_plan(data, project_id=TestProjectsArrangementPlansValidation.project_id)
        assert response.status_code == 200
        response_json = response.json()
        TestProjectsArrangementPlansValidation.pid = response_json['id']
        document_id = response_json['document']['id']
        document_name = response_json['document']['lastVersion']['name']

        response = self.rest.get_projects_id_arrangement_plan(TestProjectsArrangementPlansValidation.project_id)
        response_json = response.json()
        assert_key_in_json_array(response_json, key='name', value=data['name'])

        # document = Document(id=document_id, document_status=4, project=TestProjectsArrangementPlansValidation.project_id, document_type=6, name=document_name)
        # утвердить документ ПМа
        # response = self.rest.put_document(document)
        # assert response.status_code == 200


    # stages ------------------
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans validation')
    @allure.story('POST /projects/:id/arrangementPlans/:planId/stages')
    @pytest.mark.run(after='test_projects_put_arrangement_plans')
    def test_projects_post_arrangement_plans_stage(self):

        data = dict()
        data['endDate'] = get_unix_time_in_future(1)
        data['name'] = random_word(5)
        data['number'] = random_int(5)
        data['projectArrangementPlan'] = TestProjectsArrangementPlansValidation.pid
        response = self.rest.post_projects_id_arrangement_plan_id_stage(data, project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid)
        assert response.status_code == 200
        TestProjectsArrangementPlansValidation.stage_id = response.json()['id']

        response = self.rest.get_projects_id_arrangement_plan_id_stage(project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid)

        assert response.status_code == 200

        assert_node_in_json(response.json(), data)

    # arrangements ------------------
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangements validation')
    @allure.story('POST /arrangements')
    @pytest.mark.run(after='test_projects_get_id_arrangement_plans_stage_id')
    def test_projects_post_arrangements(self):

        data = dict()
        data['number'] = random_int(5)
        data['planId'] = TestProjectsArrangementPlansValidation.pid
        data['name'] = random_word(5)
        data['completionDate'] = get_unix_time()
        data['mainExecutor'] = TestProjectsArrangementPlansValidation.main_executor
        data['arrangementPlanStage'] = TestProjectsArrangementPlansValidation.stage_id
        response = self.rest.post_arrangements(data)
        assert response.status_code == 200
        TestProjectsArrangementPlansValidation.arrangement_id = response.json()['id']

        response = self.rest.get_arrangements_id(TestProjectsArrangementPlansValidation.arrangement_id)
        assert response.status_code == 200

        assert_node_in_json(response.json(), data)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans validation')
    @allure.story('DELETE /projects/:id/arrangementPlans/:planId/stages/:stageId')
    @pytest.mark.run(after='test_projects_post_arrangements')
    def test_projects_delete_arrangement_plans_stage(self):

        response = self.rest.delete_projects_id_arrangement_plan_stage_id(project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid, stage_id=TestProjectsArrangementPlansValidation.stage_id)
        message = response.json()
        assert response.status_code == 400
        response = self.rest.get_projects_id_arrangement_plan_id_stage(project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid)

        stages = list()
        for item in response.json():
            for arr in item['arrangements']:
                stages.append(arr['arrangementPlanStage'])
        assert TestProjectsArrangementPlansValidation.stage_id in stages
        assert message['type'] == 'actionNeeded'
        warning = unicode('Этап содержит мероприятия.', 'utf-8')
        assert message['message'] == warning

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans validation')
    @allure.story('DELETE /projects/:id/arrangementPlans/:planId/stages/:stageId/?action = move')
    @pytest.mark.run(after='test_projects_delete_arrangement_plans_stage')
    def test_projects_delete_arrangement_plans_stage_action_move(self):

        response = self.rest.delete_projects_id_arrangement_plan_stage_id(project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid, stage_id=TestProjectsArrangementPlansValidation.stage_id, action='?action=move')
        assert response.status_code == 200
        response = self.rest.get_projects_id_arrangement_plan_id_stage(project_id=TestProjectsArrangementPlansValidation.project_id, plan_id=TestProjectsArrangementPlansValidation.pid)
        response = self.rest.get_arrangements()
        print TestProjectsArrangementPlansValidation.arrangement_id
        arr = list()
        for item in response.json():
            arr.append(item['id'])
        assert TestProjectsArrangementPlansValidation.arrangement_id in arr

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangements')
    @allure.story('DELETE /arrangements/:id')
    @pytest.mark.run(after='test_projects_delete_arrangement_plans_stage')
    def test_projects_delete_arrangement(self):

        response = self.rest.delete_arrangements_id(TestProjectsArrangementPlansValidation.arrangement_id)
        assert response.status_code == 200
        response = self.rest.get_arrangements()
        for item in response.json():
            assert item['id'] != TestProjectsArrangementPlansValidation.arrangement_id













































