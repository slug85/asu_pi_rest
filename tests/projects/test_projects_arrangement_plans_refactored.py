# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.project import *
from classes.calendar_plan import *


class TestProjectsArrangementPlans:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema_PM_post_response.json", 'r').read())
        cls.schema_stage = json.loads(open(cls.current_dir + "/schema_PM_post_stage_response.json", 'r').read())
        cls.schema_get = json.loads(open(cls.current_dir + "/schema_PM_get.json", 'r').read())
        cls.schema_get_id = json.loads(open(cls.current_dir + "/schema_PM_get_id.json", 'r').read())
        cls.schema_get_stages = json.loads(open(cls.current_dir + "/schema_PM_get_stage_response.json", 'r').read())
        cls.schema_get_stages_id = json.loads(
            open(cls.current_dir + "/schema_PM_get_stage_id_response.json", 'r').read())
        cls.schema_get_arrangements_id = json.loads(
            open(cls.current_dir + "/schema_arrangement_get_id.json", 'r').read())
        cls.schema_get_arrangements = json.loads(open(cls.current_dir + "/schema_arrangement_get.json", 'r').read())
        cls.schema_status_report = json.loads(open(cls.current_dir + "/schema_pm_status_reports.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('POST /projects/:id/arrangementPlans')
    def test_projects_post_arrangement_plans(self):

        # добавление проекта
        project = Project()
        response = self.rest.post_projects(project.json())
        project_id = response.json()['id']
        TestProjectsArrangementPlans.project_id = project_id

        # добавление пма
        pm = ArrangementPlan(control_date=get_unix_time_in_future(2), project_id=project_id, control_level=2)
        response = self.rest.post_projects_id_arrangement_plan(pm.json(), project_id)
        plan_id = response.json()['id']

        TestProjectsArrangementPlans.plan_id = plan_id

        # добавление вехи
        milestone = Milestone(project_id=project_id)
        self.rest.post_milestones(milestone.json())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET /projects/:id/arrangementPlans')
    @pytest.mark.run(after='test_projects_post_arrangement_plans')
    def test_projects_get_arrangement_plans(self):
        response = self.rest.get_projects_id_arrangement_plan(TestProjectsArrangementPlans.project_id)
        response_json = response.json()
        assert response.status_code == 200

        validate_json(response_json, self.schema_get)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET /projects/:id/arrangementPlans/:planId')
    @pytest.mark.run(after='test_projects_post_arrangement_plans')
    def test_projects_get_arrangement_plans_id(self):
        response = self.rest.get_projects_id_arrangement_plan(TestProjectsArrangementPlans.project_id)
        response_json = response.json()
        assert response.status_code == 200

        validate_json(response_json, self.schema_get_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('PUT /projects/:id/arrangementPlans/:planId')
    @pytest.mark.run(after='test_projects_get_arrangement_plans_id')
    def test_projects_put_arrangement_plans(self):

        # изменение пма
        pm = ArrangementPlan(control_date=get_unix_time_in_future(3), project_id=TestProjectsArrangementPlans.project_id, control_level=3)
        response = self.rest.put_projects_id_arrangement_plan(pm.json(), project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.plan_id)

        assert response.status_code == 200
        assert response.json()['name'] == pm.name
        assert response.json()['controlDate'] == pm.controlDate

    # ЭТАП ПМ
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('POST /projects/:id/arrangementPlans/:planId/stages')
    @pytest.mark.run(after='test_projects_put_arrangement_plans')
    def test_projects_post_arrangement_plans_stage(self):

        stage = ArrangementPlanStage(end_date=get_unix_time(), arrangement_plan=TestProjectsArrangementPlans.plan_id)

        response = self.rest.post_projects_id_arrangement_plan_id_stage(stage.json(), project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.plan_id)
        TestProjectsArrangementPlans.stage_id = response.json()['id']
        TestProjectsArrangementPlans.stage = stage
        assert response.status_code == 200
        assert response.json()['name'] == stage.name

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET     /projects/:id/arrangementPlans/:planId/stagesBeforeDate/:date')
    @pytest.mark.run(after='test_projects_post_arrangement_plans_stage')
    def test_projects_get_arrangement_plans_stage_before_date(self):
        response = self.rest.get_arrangement_plan_stage_before_date(project_id=TestProjectsArrangementPlans.project_id,
                                                                    plan_id=TestProjectsArrangementPlans.plan_id,
                                                                    date=TestProjectsArrangementPlans.stage.endDate)
        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_stages)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('PUT /projects/:id/arrangementPlans/:planId/stages/:stageId')
    @pytest.mark.run(after='test_projects_post_arrangement_plans_stage')
    def test_projects_put_arrangement_plans_stage(self):
        stage = ArrangementPlanStage(end_date=get_unix_time(), arrangement_plan=TestProjectsArrangementPlans.plan_id)

        response = self.rest.put_projects_id_arrangement_plan_id_stage(stage.json(), project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.plan_id, stage_id=TestProjectsArrangementPlans.stage_id)
        assert response.status_code == 200
        assert response.json()['name'] == stage.name

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET /projects/:id/arrangementPlans/:planId/stages')
    @pytest.mark.run(after='test_projects_put_arrangement_plans_stage')
    def test_projects_get_id_arrangement_plans_stage(self):
        response = self.rest.get_projects_id_arrangement_plan_id_stage(
            project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.plan_id)
        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_stages)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET /projects/:id/arrangementPlans/:planId/stages/:stageId')
    @pytest.mark.run(after='test_projects_put_arrangement_plans_stage')
    def test_projects_get_id_arrangement_plans_stage_id(self):
        response = self.rest.get_projects_id_arrangement_plan_id_stage_id(
            project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.plan_id,
            stage_id=TestProjectsArrangementPlans.stage_id)
        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_stages_id)
