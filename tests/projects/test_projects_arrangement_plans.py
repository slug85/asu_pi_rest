# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time


class TestProjectsArrangementPlans:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema_PM_post_response.json", 'r').read())
        cls.schema_stage = json.loads(open(cls.current_dir + "/schema_PM_post_stage_response.json", 'r').read())
        cls.schema_get = json.loads(open(cls.current_dir + "/schema_PM_get.json", 'r').read())
        cls.schema_get_id = json.loads(open(cls.current_dir + "/schema_PM_get_id.json", 'r').read())
        cls.schema_get_stages = json.loads(open(cls.current_dir + "/schema_PM_get_stage_response.json", 'r').read())
        cls.schema_get_stages_id = json.loads(
            open(cls.current_dir + "/schema_PM_get_stage_id_response.json", 'r').read())
        cls.schema_get_arrangements_id = json.loads(
            open(cls.current_dir + "/schema_arrangement_get_id.json", 'r').read())
        cls.schema_get_arrangements = json.loads(open(cls.current_dir + "/schema_arrangement_get.json", 'r').read())
        cls.schema_status_report = json.loads(open(cls.current_dir + "/schema_pm_status_reports.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('POST /projects/:id/arrangementPlans')
    def test_projects_post_arrangement_plans(self):
        TestProjectsArrangementPlans.project_id = self.rest.get_projects().json()[-1]['id']
        TestProjectsArrangementPlans.organization_id = self.rest.get_organizations().json()[-1]['id']
        TestProjectsArrangementPlans.main_executor = self.rest.get_organizations().json()[-1]

        # добавление вехи
        data = dict()
        data['name'] = random_name_with_date(5)
        data['plannedCompletionDate'] = get_unix_time()
        data['project'] = TestProjectsArrangementPlans.project_id
        data['confirm'] = True
        self.rest.post_milestones(data)

        controlOrganization = dict()
        data = dict()
        data['projectId'] = TestProjectsArrangementPlans.project_id
        data['name'] = random_word(random_int(10))
        data['controlDate'] = get_unix_time_in_future(40)
        data['controlLevel'] = 2
        data['description'] = random_name_with_date()
        data['projectLifecycleStage'] = {"id": 1}
        data['projectState'] = {"id": 1}
        data['projectStatus'] = {"id": 1}
        data['projectType'] = {"id": 1}
        data['subprogram'] = {"id": 1}
        data['projectCoordinator'] = {"id": self.rest.get_users().json()[0]['id']}

        controlOrganization['controlOrganization'] = {"id": TestProjectsArrangementPlans.organization_id}
        controlOrganization['controlDate'] = get_unix_time_in_future(40)
        controlOrganization['description'] = random_name_with_date()
        controlOrganization['files'] = []

        data['controlOrganizations'] = [controlOrganization]
        data['arrangementPlanControls'] = [controlOrganization]

        data['files'] = []

        response = self.rest.post_projects_id_arrangement_plan(data, project_id=TestProjectsArrangementPlans.project_id)
        assert response.status_code == 200
        response_json = response.json()
        TestProjectsArrangementPlans.pid = response_json['id']

        validate_json(response_json, self.schema)
        response = self.rest.get_projects_id_arrangement_plan(TestProjectsArrangementPlans.project_id)
        response_json = response.json()

        assert_key_in_json_array(response_json, key='name', value=data['name'])

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET /projects/:id/arrangementPlans')
    @pytest.mark.run(after='test_projects_post_arrangement_plans')
    def test_projects_get_arrangement_plans(self):
        response = self.rest.get_projects_id_arrangement_plan(TestProjectsArrangementPlans.project_id)
        response_json = response.json()
        assert response.status_code == 200

        validate_json(response_json, self.schema_get)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET /projects/:id/arrangementPlans/:planId')
    @pytest.mark.run(after='test_projects_post_arrangement_plans')
    def test_projects_get_arrangement_plans_id(self):
        response = self.rest.get_projects_id_arrangement_plan(TestProjectsArrangementPlans.project_id)
        response_json = response.json()
        assert response.status_code == 200

        validate_json(response_json, self.schema_get_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('PUT /projects/:id/arrangementPlans/:planId')
    @pytest.mark.run(after='test_projects_get_arrangement_plans_id')
    def test_projects_put_arrangement_plans(self):
        controlOrganization = dict()
        data = dict()
        data['projectId'] = TestProjectsArrangementPlans.project_id
        data['name'] = random_word(random_int(10))
        data['controlDate'] = get_unix_time_in_future(30)
        data['controlLevel'] = 1
        data['description'] = random_name_with_date()
        data['projectCoordinator'] = {"id": self.rest.get_users().json()[0]['id']}

        controlOrganization['controlOrganization'] = {"id": TestProjectsArrangementPlans.organization_id}
        controlOrganization['controlDate'] = get_unix_time_in_future(30)
        controlOrganization['description'] = random_name_with_date()
        controlOrganization['files'] = []

        data['controlOrganizations'] = [controlOrganization]
        data['arrangementPlanControls'] = [controlOrganization]

        data['files'] = []

        TestProjectsArrangementPlans.arrangement_plan = data

        response = self.rest.put_projects_id_arrangement_plan(data, project_id=TestProjectsArrangementPlans.project_id,
                                                              plan_id=TestProjectsArrangementPlans.pid)
        response_json = response.json()
        TestProjectsArrangementPlans.arrangement_plan_id = response_json['id']
        # validate_json(response_json, self.schema)
        assert response.status_code == 200
        assert_node_in_json(response_json, ['name', 'description'], data)

    # stages ------------------
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('POST /projects/:id/arrangementPlans/:planId/stages')
    @pytest.mark.run(after='test_projects_put_arrangement_plans')
    def test_projects_post_arrangement_plans_stage(self):
        data = dict()
        data['endDate'] = get_unix_time_in_future(1)
        data['name'] = random_word(5)
        data['number'] = 1
        data['projectArrangementPlan'] = TestProjectsArrangementPlans.arrangement_plan
        response = self.rest.post_projects_id_arrangement_plan_id_stage(data,
                                                                        project_id=TestProjectsArrangementPlans.project_id,
                                                                        plan_id=TestProjectsArrangementPlans.pid)
        assert response.status_code == 200
        assert response.json()['number'] == data['number']
        TestProjectsArrangementPlans.stage_id = response.json()['id']
        TestProjectsArrangementPlans.stage_date = response.json()['endDate']
        validate_json(response.json(), self.schema_stage)

        response = self.rest.get_projects_id_arrangement_plan_id_stage(
            project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.pid)

        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_stages)

        assert_node_in_json(response.json(), data)

    # stages ------------------
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET     /projects/:id/arrangementPlans/:planId/stagesBeforeDate/:date')
    @pytest.mark.run(after='test_projects_post_arrangement_plans_stage')
    def test_projects_get_arrangement_plans_stage_before_date(self):
        response = self.rest.get_arrangement_plan_stage_before_date(project_id=TestProjectsArrangementPlans.project_id,
                                                                    plan_id=TestProjectsArrangementPlans.pid,
                                                                    date=TestProjectsArrangementPlans.stage_date)
        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_stages)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('PUT /projects/:id/arrangementPlans/:planId/stages/:stageId')
    @pytest.mark.run(after='test_projects_post_arrangement_plans_stage')
    def test_projects_put_arrangement_plans_stage(self):
        data = dict()
        data['endDate'] = get_unix_time_in_future(1)
        data['name'] = random_word(5)
        data['projectArrangementPlan'] = TestProjectsArrangementPlans.arrangement_plan
        response = self.rest.put_projects_id_arrangement_plan_id_stage(data,
                                                                       project_id=TestProjectsArrangementPlans.project_id,
                                                                       plan_id=TestProjectsArrangementPlans.pid,
                                                                       stage_id=TestProjectsArrangementPlans.stage_id)
        assert response.status_code == 200

        validate_json(response.json(), self.schema_stage)
        response = self.rest.get_projects_id_arrangement_plan_id_stage(
            project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.pid)

        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_stages)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET /projects/:id/arrangementPlans/:planId/stages')
    @pytest.mark.run(after='test_projects_put_arrangement_plans_stage')
    def test_projects_get_id_arrangement_plans_stage(self):
        response = self.rest.get_projects_id_arrangement_plan_id_stage(
            project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.pid)
        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_stages)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET /projects/:id/arrangementPlans/:planId/stages/:stageId')
    @pytest.mark.run(after='test_projects_put_arrangement_plans_stage')
    def test_projects_get_id_arrangement_plans_stage_id(self):
        response = self.rest.get_projects_id_arrangement_plan_id_stage_id(
            project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.pid,
            stage_id=TestProjectsArrangementPlans.stage_id)
        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_stages_id)

    # arrangements
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangements')
    @allure.story('POST /arrangements')
    @pytest.mark.run(after='test_projects_get_id_arrangement_plans_stage_id')
    def test_projects_post_arrangements(self):
        data = dict()
        data['number'] = 1
        data['planId'] = TestProjectsArrangementPlans.arrangement_plan_id
        data['name'] = random_word(5)
        data['completionDate'] = get_unix_time()
        data['mainExecutor'] = TestProjectsArrangementPlans.main_executor
        data['arrangementPlanStage'] = TestProjectsArrangementPlans.stage_id
        response = self.rest.post_arrangements(data)

        # статус-отчет по мероприятию id
        TestProjectsArrangementPlans.arrangement_status_report_id = response.json()['statusReport']['id']
        assert response.json()['number'] == data['number']

        assert response.status_code == 200
        TestProjectsArrangementPlans.arrangement_id = response.json()['id']

        response = self.rest.get_arrangements_id(TestProjectsArrangementPlans.arrangement_id)
        assert response.status_code == 200

        assert_node_in_json(response.json(), data)

        validate_json(response.json(), self.schema_get_arrangements_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangements')
    @allure.story('POST /arrangements (out of stage)')
    @pytest.mark.run(after='test_projects_post_arrangements')
    def test_projects_post_arrangements_out_of_stage(self):
        data = dict()
        data['number'] = 2
        data['name'] = random_word(5)
        data['completionDate'] = get_unix_time()
        data['mainExecutor'] = TestProjectsArrangementPlans.main_executor
        data['planId'] = TestProjectsArrangementPlans.pid

        response = self.rest.post_arrangements(data)
        assert response.status_code == 200
        TestProjectsArrangementPlans.arrangement_id_out_of_stage = response.json()['id']

        assert response.json()['number'] == data['number']

        response = self.rest.get_arrangements_id(TestProjectsArrangementPlans.arrangement_id_out_of_stage)
        assert response.status_code == 200

        assert_node_in_json(response.json(), data)

        validate_json(response.json(), self.schema_get_arrangements_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangements')
    @allure.story('PUT /arrangements/:id')
    @pytest.mark.run(after='test_projects_post_arrangements_out_of_stage')
    def test_projects_put_arrangements(self):
        data = dict()
        data['name'] = random_word(5)
        data['planId'] = TestProjectsArrangementPlans.arrangement_plan_id
        data['completionDate'] = get_unix_time()
        data['mainExecutor'] = TestProjectsArrangementPlans.main_executor
        data['arrangementPlanStage'] = TestProjectsArrangementPlans.stage_id
        response = self.rest.put_arrangements(data, TestProjectsArrangementPlans.arrangement_id)
        assert response.status_code == 200
        TestProjectsArrangementPlans.arrangement_id = response.json()['id']

        response = self.rest.get_arrangements_id(TestProjectsArrangementPlans.arrangement_id)
        assert response.status_code == 200

        assert_node_in_json(response.json(), data)

        validate_json(response.json(), self.schema_get_arrangements_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangements')
    @allure.story('GET /arrangements/:id')
    @pytest.mark.run(after='test_projects_put_arrangements')
    def test_projects_get_arrangements_id(self):
        response = self.rest.get_arrangements_id(TestProjectsArrangementPlans.arrangement_id)
        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_arrangements_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangements')
    @allure.story('GET /arrangements')
    @pytest.mark.run(after='test_projects_get_arrangements_id')
    def test_projects_get_arrangements(self):
        response = self.rest.get_arrangements()
        assert response.status_code == 200
        validate_json(response.json(), self.schema_get_arrangements)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('GET     /projects/:id/arrangementPlans/:planId/statusReports')
    @pytest.mark.run(after='test_projects_get_arrangements')
    def test_projects_get_id_arrangement_plans_statusreport_id(self):
        response = self.rest.get_projects_id_arrangement_plan_id_statusreport_id(
            project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.pid)
        assert response.status_code == 200
        validate_json(response.json(), self.schema_status_report)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET /projects/:id/arrangementPlans/:planId/report')
    @pytest.mark.run(after='test_projects_get_id_arrangement_plans_statusreport_id')
    def test_projects_get_arrangement_plans_report(self):
        response = self.rest.get_arrangement_plans_report(project_id=TestProjectsArrangementPlans.project_id,
                                                          plan_id=TestProjectsArrangementPlans.pid)
        check_report_response(response)

    # arrangements status-report
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/statusreports')
    @allure.story('POST     /statusreports')
    @pytest.mark.run(after='test_projects_arrangement_plans_status_report_filters')
    def test_projects_post_arrangements_status_report(self):

        response = self.rest.get_statusreports_id(TestProjectsArrangementPlans.arrangement_status_report_id)
        assert response.status_code == 200

        # post status report for arrangement
        text = random_word(50)
        status_report_arrangement = {"id": TestProjectsArrangementPlans.arrangement_status_report_id,
                                     "entry": {"report": text},
                                     'type': 'arrangement'}

        response = self.rest.put_statusreports(id=TestProjectsArrangementPlans.arrangement_status_report_id,
                                               data=status_report_arrangement)
        assert response.status_code == 200

        # проверяю тип сохраненного статус отчета
        response = self.rest.get_statusreports_id(TestProjectsArrangementPlans.arrangement_status_report_id)
        assert response.json()['entries'][0]['report'] == text
        assert response.json()['entries'][0]['type'] == 'arrangement'

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangements')
    @allure.story('DELETE /arrangements/:id')
    @pytest.mark.run(after='test_projects_arrangement_plans_status_report_filters')
    def test_projects_delete_arrangement(self):

            response = self.rest.delete_arrangements_id(TestProjectsArrangementPlans.arrangement_id)
            assert response.status_code == 200
            response = self.rest.get_arrangements()
            for item in response.json():
                assert item['id'] != TestProjectsArrangementPlans.arrangement_id

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/arrangementPlans')
    @allure.story('DELETE /projects/:id/arrangementPlans/:planId/stages/:stageId')
    @pytest.mark.run('second-to-last')
    def test_projects_delete_arrangement_plans_stage(self):

            response = self.rest.delete_projects_id_arrangement_plan_stage_id(
                project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.pid,
                stage_id=TestProjectsArrangementPlans.stage_id)
            assert response.status_code == 200
            response = self.rest.get_projects_id_arrangement_plan_id_stage(
                project_id=TestProjectsArrangementPlans.project_id, plan_id=TestProjectsArrangementPlans.pid)
            for item in response.json():
                assert item['arrangementPlanStage'] != TestProjectsArrangementPlans.stage_id












































