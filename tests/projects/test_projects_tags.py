# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from sender.request_sender import Sender
from classes.project import *
from classes.calendar_plan import CalendarPlanStage,CalendarPlan


class TestProjectWihTags:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.sender = Sender()

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('POST     /projects')
    @pytest.mark.first
    def test_projects_post_with_tag(self):

        tag1 = ProjectTag()
        tag_id = self.rest.post_projecttags(tag1.json()).json()['id']
        tag_label = tag1.label
        tag_isActive = self.rest.post_projecttags(tag1.json()).json()['isActive']

        # post project with tags
        tag = {"id": tag_id, "label": tag_label, "isActive": tag_isActive}
        project = Project(project_tags=tag)

        response = self.rest.post_projects(project.json())
        assert response.status_code == 200
        assert response.json()['tags'][0]['id'] == tag_id

        project_id = response.json()['id']
        response = self.rest.get_projects_id(project_id,mode='full')
        assert response.status_code == 200
        assert response.json()['tags'][0]['id'] == tag_id






























