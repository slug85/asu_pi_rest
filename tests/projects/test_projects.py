# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from sender.request_sender import Sender
from classes.project import Project
from classes.calendar_plan import CalendarPlanStage,CalendarPlan


class TestProjects:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())
        cls.schema_calendar = json.loads(open(cls.current_dir + "/schema_calendar_plan.json", 'r').read())
        cls.schema_work_plan = json.loads(open(cls.current_dir + "/schema_work_plan.json", 'r').read())
        cls.schema_roots = json.loads(open(cls.current_dir + "/schema_roots.json", 'r').read())
        cls.schema_work_plan_milestone = json.loads(
            open(cls.current_dir + "/schema_calendar_plan_milestone.json", 'r').read())
        cls.sender = Sender()

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('POST     /projects')
    @pytest.mark.first
    def test_projects_post(self):

        project = Project()
        response = self.rest.post_projects(project.json())
        assert response.status_code == 200

        response_json = response.json()

        assert project.name == response_json['name']
        assert project.shortName == response_json['shortName']
        assert project.statusReport.period == response_json['statusReport']['period']

        TestProjects.id = response_json['id']
        project.id = TestProjects.id
        TestProjects.project = project

        response = self.rest.get_projects_id(TestProjects.id)
        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('PUT     /projects')
    @pytest.mark.run(after='test_projects_post')
    def test_projects_put(self):

        project = Project()
        response = self.rest.put_projects(data=project.json(), id=TestProjects.id)
        assert response.status_code == 200

        response_json = response.json()

        assert project.name == response_json['name']
        assert project.shortName == response_json['shortName']
        assert project.statusReport.period == response_json['statusReport']['period']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects?mode=full')
    @pytest.mark.run(after='test_projects_put')
    def test_projects_get(self):
        response = self.rest.get_projects(mode='full')
        response_json = response.json()
        assert response.status_code == 200
        validate_json(response_json, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/:id')
    @pytest.mark.run(after='test_projects_put')
    def test_projects_get_id(self):
        response = self.rest.get_projects_id(TestProjects.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/:id/calendarPlan')
    @pytest.mark.run(after='test_projects_get_id')
    def test_projects_get_id_calendarplan(self):

        # этап КП
        calendar_plan_stage = CalendarPlanStage(end_date=get_unix_time_in_past(2))

        # КП
        calendar_plan = CalendarPlan(project_id=TestProjects.id, calendar_plan_stages=[calendar_plan_stage.json()]).json()

        response = self.rest.put_calendarPlan(calendar_plan, project_id=TestProjects.id)
        assert response.status_code == 200

        response = self.rest.get_projects_id_calendar(TestProjects.id)
        assert response.status_code == 200

        validate_json(response.json(), self.schema_calendar)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/:id/workPlan')
    @pytest.mark.run(after='test_projects_get_id')
    def test_projects_get_id_work_plan(self):

        response = self.rest.get_projects_id_workPlan(project_id=TestProjects.id)
        assert response.status_code == 200

        TestProjects.milestone_before_date = response.json()['byStages']['stagesAndMilestones'][0]['milestones'][0]['plannedCompletionDate']

        validate_json(response.json(), self.schema_work_plan)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET     /projects/:id/workPlan/milestonesBeforeDate/:date')
    @pytest.mark.run(after='test_projects_get_id_work_plan')
    def test_projects_get_id_work_plan_milestones_before_date(self):
        milestone_before_date = TestProjects.milestone_before_date
        response = self.rest.get_projects_id_workPlan_milestone_before_date(project_id=TestProjects.id,
                                                                            date=milestone_before_date)

        assert response.status_code == 200
        validate_json(response.json(), self.schema_work_plan_milestone)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET   /projects/:id/workPlan/milestonesForStatusReport')
    @pytest.mark.run(after='test_projects_get_id_work_plan_milestones_before_date')
    def test_projects_get_id_work_plan_milestones_for_status_report(self):
        response = self.rest.get_projects_id_workPlan_milestone_status_report(project_id=TestProjects.id)
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET /projects/roots')
    def test_projects_get_roots(self):
        pytest.skip('роут выпилен')
        response = self.rest.get_projects_roots()
        assert response.status_code == 200

        validate_json(response.json(), self.schema_roots)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET /projects/reportPivot ')
    def test_projects_get_report_pivot(self):
        response = self.rest.get_projects_report('reportPivot')
        check_report_response(response)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET /projects/reportByCoordinator')
    def test_projects_get_report_by_reportByCoordinator(self):
        response = self.rest.get_projects_report('reportByCoordinator')
        check_report_response(response)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET /projects/reportProjectsByMainCustomer')
    def test_projects_get_reportProjectsByMainCustomer(self):
        response = self.rest.get_projects_report('reportByMainCustomer')
        check_report_response(response)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET /projects/reportProjectsByMainExecutor')
    def test_projects_get_reportProjectsByMainExecutor(self):
        pytest.skip('neaktualnij test')
        response = self.rest.get_projects_report('reportByMainExecutor')
        check_report_response(response)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET /projects/reportCompound')
    def test_projects_get_report_compound(self):
        response = self.rest.get_projects_report('reportCompound')
        check_report_response(response)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET /projects/:id/workPlan/report ')
    def test_projects_get_report_work_plan(self):
        response = self.rest.get_projects_workplan_report(1)
        check_report_response(response)

    @allure.feature('/projects')
    @allure.story('GET /projects/:id/calendarplan/report ')
    def test_projects_get_report_calendar_plan(self):
        response = self.rest.get_projects_calendarplan_report(1)
        check_report_response(response)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('GET /projects/:id/reportStatusReports')
    @pytest.mark.run(after='test_projects_get_id_work_plan_milestones_before_date')
    def test_projects_get_report_status_reports(self):
        response = self.rest.get_report_status_reports(project_id=TestProjects.id)
        check_report_response(response)


























