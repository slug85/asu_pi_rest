# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from sender.request_sender import Sender
from classes.project import Project


class TestProjectsPriice:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.sender = Sender()

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('POST     /projects')
    @pytest.mark.first
    def test_projects_post_with_price(self):
        pytest.skip('включить после мержда')
        project = Project()
        project.price = random_int(1000000)
        response = self.rest.post_projects(project.json())

        assert response.status_code == 200

        response_json = response.json()
        TestProjectsPriice.project_id = response.json()['id']

        assert project.name == response_json['name']
        assert project.shortName == response_json['shortName']
        assert project.price == response_json['price']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projects')
    @allure.story('POST     /projects')
    @pytest.mark.second
    def test_projects_put_with_price(self):

        pytest.skip('включить после мержда')
        project = Project()
        project.price = random_int(1000000)
        response = self.rest.put_projects(project.json(), TestProjectsPriice.project_id)
        response_json = response.json()

        assert project.name == response_json['name']
        assert project.shortName == response_json['shortName']
        assert project.price == response_json['price']






















