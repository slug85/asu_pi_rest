# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.subprogram import *


class Test_Subprograms:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_subprograms()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/subprograms')
    @allure.story('POST     /subprograms')
    @pytest.mark.first
    def test_subprograms_post(self):

        subprogram = Subprogram()

        response = self.rest.post_subprograms(subprogram.json())
        assert response.status_code == 200
        assert response.json()['actual'] is True
        response_json = response.json()
        Test_Subprograms.id = response_json['id']

        response = self.rest.get_subprograms_id(Test_Subprograms.id)
        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/subprograms')
    @allure.story('POST     /subprograms actual state')
    @pytest.mark.first
    def test_subprograms_post_default_actual_state(self):

        subprogram = Subprogram()
        subprogram.actual = None

        response = self.rest.post_subprograms(subprogram.json())
        assert response.status_code == 200
        assert response.json()['actual'] is True

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/subprograms')
    @allure.story('PUT     /subprograms')
    @pytest.mark.run(after='test_subprograms_post')
    def test_subprograms_put(self):

        subprogram = Subprogram()

        response = self.rest.put_subprograms(data=subprogram.json(), id=Test_Subprograms.id)
        assert response.status_code == 200

        assert response.json()['name'] == subprogram.name
        assert response.json()['shortName'] == subprogram.shortName

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/subprograms')
    @allure.story('GET     /subprograms')
    @pytest.mark.run(after='test_subprograms_put')
    def test_subprograms_get(self):
        assert self.response.status_code == 200
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/subprograms')
    @allure.story('GET     /subprograms/:id')
    @pytest.mark.run(after='test_subprograms_put')
    def test_subprograms_get_id(self):
        response = self.rest.get_subprograms_id(Test_Subprograms.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/subprograms')
    @allure.story('GET programs/:id/subprograms')
    @pytest.mark.run(after='test_subprograms_put')
    def test_programs_get_subprograms(self):

        # добавление программы
        program = Program()
        response = self.rest.post_programs(program.json())
        assert response.status_code == 200
        program_id = response.json()['id']

        # добавление подпрограммы в ней
        subprogram = Subprogram(program_id=program_id, selected_program_id=program_id)
        response = self.rest.post_subprograms(subprogram.json())
        assert response.status_code == 200

        # проверка роута
        response = self.rest.get_programs_id_subprograms(program_id)
        assert response.status_code == 200
        assert subprogram.name in response.json()[0]['name']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/subprograms')
    @allure.story('DELETE  /subprograms/:id')
    @pytest.mark.last
    def test_subprograms_delete(self):
        response = self.rest.delete_subprograms_id(Test_Subprograms.id)
        assert response.status_code == 200

        response = self.rest.get_subprograms()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', Test_Subprograms.id)