# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time

class TestProjectFoundationType:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_projectfoundationtypes()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectfoundationtypes')
    @allure.story('POST     /projectfoundationtypes')
    @pytest.mark.first
    def test_projectfoundationtypes_post(self):

        data = dict()
        data['description'] = random_word(25)
        data['name'] = random_word(7)
        data['shortName'] = random_word(3)

        response = self.rest.post_projectfoundationtypes(data)
        assert response.status_code == 200
        response_json = response.json()
        TestProjectFoundationType.id = response_json['id']

        assert_that_json_has_data(response_json, data)
        response = self.rest.get_projectfoundationtypes_id(TestProjectFoundationType.id)
        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

        assert_that_json_has_data(response_json, data)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectfoundationtypes')
    @allure.story('PUT     /projectfoundationtypes')
    @pytest.mark.run(after='test_projectfoundationtypes_post')
    def test_projectfoundationtypes_put(self):

        data = dict()
        data['description'] = random_word(25)
        data['name'] = random_word(7)
        data['shortName'] = random_word(3)
        response = self.rest.put_projectfoundationtypes(data=data, id=TestProjectFoundationType.id)
        assert response.status_code == 200

        response_json = response.json()
        assert_that_json_has_data(response_json, data)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectfoundationtypes')
    @allure.story('GET     /projectfoundationtypes')
    @pytest.mark.run(after='test_projectfoundationtypes_put')
    def test_projectfoundationtypes_get(self):

        assert self.response.status_code == 200
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectfoundationtypes')
    @allure.story('GET     /projectfoundationtypes/:id')
    @pytest.mark.run(after='test_projectfoundationtypes_put')
    def test_projectfoundationtypes_get_id(self):

        response = self.rest.get_projectfoundationtypes_id(TestProjectFoundationType.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectfoundationtypes')
    @allure.story('DELETE  /projectfoundationtypes/:id')
    @pytest.mark.last
    def test_projectfoundationtypes_delete(self):

        response = self.rest.delete_projectfoundationtypes_id(TestProjectFoundationType.id)
        assert response.status_code == 200

        response = self.rest.get_projectfoundationtypes()
        response_json = response.json()

        assert_key_not_in_json_array(response_json,'id', TestProjectFoundationType.id)