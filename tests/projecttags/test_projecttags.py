# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.project import *

class TestProjectTags:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_projecttags()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projecttags')
    @allure.story('POST     /projecttags')
    @pytest.mark.first
    def test_projecttags_post(self):

        project_tag = ProjectTag()

        response = self.rest.post_projecttags(project_tag.json())
        assert response.status_code == 200
        response_json = response.json()
        TestProjectTags.id = response_json['id']

        response = self.rest.get_projecttags_id(TestProjectTags.id)
        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projecttags')
    @allure.story('PUT     /projecttags')
    @pytest.mark.run(after='test_projecttags_post')
    def test_projecttags_put(self):

        project_tag = ProjectTag()

        response = self.rest.put_projecttags(data=project_tag.json(), id=TestProjectTags.id)
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projecttags')
    @allure.story('GET     /projecttags')
    @pytest.mark.run(after='test_projecttags_put')
    def test_projecttags_get(self):

        response = self.rest.get_projecttags()
        assert response.status_code == 200
        validate_json(response.json(), self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projecttags')
    @allure.story('GET     /projecttags/:id')
    @pytest.mark.run(after='test_projecttags_put')
    def test_projecttags_get_id(self):

        response = self.rest.get_projecttags_id(TestProjectTags.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projecttags')
    @allure.story('DELETE  /projecttags/:id')
    @pytest.mark.run(after='test_projecttags_get')
    def test_projecttags_delete(self):

        response = self.rest.delete_projecttags_id(TestProjectTags.id)
        assert response.status_code == 200

        response = self.rest.get_projecttags()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', TestProjectTags.id)