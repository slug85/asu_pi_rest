# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time


class TestStatusReport:
        @classmethod
        def setup_class(cls):
            cls.rest = Rest()
            cls.rest.auth()
            cls.response = cls.rest.get_statusreports()
            cls.json_dump = cls.response.json()
            cls.current_dir = os.path.dirname(__file__)
            cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

        @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
        @allure.feature('/statusreports')
        @allure.story('POST     /statusreports')
        @pytest.mark.first
        def test_statusreports_post(self):
            data = dict()
            report = random_word(50)
            data['entries'] = [{"report": report}]
            data['report'] = report

            # post
            response = self.rest.post_statusreports(data)
            response_json = response.json()
            TestStatusReport.id = response_json['id']

        @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
        @allure.feature('/statusreports')
        @allure.story('PUT     /statusreports')
        @pytest.mark.run(after='test_statusreports_post')
        def test_statusreports_put(self):

            data = dict()
            response = self.rest.put_statusreports(data=data, id=TestStatusReport.id)
            assert response.status_code == 200

            response_json = response.json()
            assert_that_json_has_data(response_json, data)

        @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
        @allure.feature('/statusreports')
        @allure.story('GET     /statusreports')
        @pytest.mark.run(after='test_statusreports_put')
        def test_statusreports_get(self):

            assert self.response.status_code == 200
            validate_json(self.json_dump[0], self.schema_id)

        @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
        @allure.feature('/statusreports')
        @allure.story('GET     /statusreports/:id')
        @pytest.mark.run(after='test_statusreports_put')
        def test_statusreports_get_id(self):

            response = self.rest.get_statusreports_id(TestStatusReport.id)
            response_json = response.json()

            assert response.status_code == 200
            validate_json(response_json, self.schema_id)

        #TODO разобраться с логикой метода
