# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time


#отключено
class MainContractors:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/maincontractors')
    @allure.story('POST     /maincontractors')
    @pytest.mark.first
    def test_maincontractors_post(self):

        # ОРГАНИЗАЦИЯ
        data = {"title": "Организация", "name": "fullName", "shortName": "", "telegraphName": "", "country": "",
                "city": "", "postIndex": "", "address": "", "comments": "", "representativeName": "",
                "representativePosition": "", "representativePhone": "", "representativeEmail": "", "contacts": [],
                "description": "", 'name': random_word(10)}
        response = self.rest.post_organizations(data)
        assert response.status_code == 200

        response_json = response.json()
        MainContractors.organization_id = response_json['id']

        # ИСПОЛНИТЕЛЬ
        data = dict()
        data['organization'] = MainContractors.organization_id
        data['name'] = random_word(7)

        response = self.rest.post_maincontractors(data)
        assert response.status_code == 200

        response_json = response.json()
        MainContractors.id = response_json['id']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/maincontractors')
    @allure.story('PUT     /maincontractors/:id ')
    @pytest.mark.run(after='test_maincontractors_post')
    def test_maincontractors_put(self):

        data = dict()
        data['organization'] = MainContractors.organization_id
        data['name'] = random_word(7)

        response = self.rest.put_maincontractors(data, MainContractors.id)
        assert response.status_code == 200
        response_json = response.json()

        assert response_json['name'] == data['name']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/maincontractors')
    @allure.story('GET     /maincontractors/:id')
    @pytest.mark.run(after='test_maincontractors_put')
    def test_maincontractors_get_id(self):

        response = self.rest.get_maincontractors_id(MainContractors.id)
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/maincontractors')
    @allure.story('GET     /maincontractors')
    @pytest.mark.run(after='test_maincontractors_put')
    def test_maincontractors_get(self):

        response = self.rest.get_maincontractors()
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/maincontractors')
    @allure.story('DELETE  /maincontractors/:id')
    @pytest.mark.last
    def test_maincontractors_delete(self):

        response = self.rest.delete_maincontractors_id(MainContractors.id)
        assert response.status_code == 200

        response = self.rest.get_maincontractors()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', MainContractors.id)













