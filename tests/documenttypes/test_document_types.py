# coding=utf-8
__author__ = 'slugovskoy'
from sender.request_sender import Sender
from helper.validator import *
from helper.generator import *
import pytest
import allure
import json
import os
import pprint
import collections

class TestDocumentTypes:

    @classmethod
    def setup_class(cls):

        cls.sender = Sender()
        cls.sender.authorize()
        cls.response = cls.sender.get('documenttypes')
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documenttypes')
    @allure.story('GET     /documenttypes')
    def test_documenttypes_get(self):

        assert self.response.status_code == 200
        TestDocumentTypes.id = self.json_dump[0]['id']

        assert_key_in_json_array(self.json_dump, 'name', 'Паспорт проекта')
        assert_key_in_json_array(self.json_dump, 'name', 'Устав проекта')
        assert_key_in_json_array(self.json_dump, 'name', 'Техническое задание')
        assert_key_in_json_array(self.json_dump, 'name', 'Календарный план')
        assert_key_in_json_array(self.json_dump, 'name', 'Основание проекта')

        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documenttypes')
    @allure.story('GET     /documenttypes/:id')
    @pytest.mark.run(after='test_documenttypes_get')
    def test_documenttypes_get_id(self):

        new_id = TestDocumentTypes.id
        response = self.sender.get('documenttypes/' + str(new_id))
        assert response.status_code == 200
        response_json = response.json()
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documenttypes')
    @allure.story('POST    /documenttypes')
    @pytest.mark.run(after='test_documenttypes_get_id')
    def test_documenttypes_post(self):

        data = collections.OrderedDict()

        data['shortName'] = random_word(5)
        data['name'] = random_word(10)
        data['maxFilesSize'] = random_int(5)
        data['maxDocuments'] = random_int(3)
        data['description'] = random_word(20)
        data['versioned'] = True
        data['agreementRequired'] = False
        data['numbered'] = True
        data['visibleInProject'] = False
        data['documentGroups'] = []

        response = self.sender.post('documenttypes', data)
        assert response.status_code == 200

        TestDocumentTypes.id = response.json()['id']

        response = self.sender.get('documenttypes/' + str(TestDocumentTypes.id))
        assert response.status_code == 200
        response_json = response.json()

        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documenttypes')
    @allure.story('PUT     /documenttypes/:id')
    @pytest.mark.run(after='test_documenttypes_post')
    def test_documenttypes_put(self):

        new_id = TestDocumentTypes.id

        data = collections.OrderedDict()
        data['shortName'] = random_word(5)
        data['name'] = random_word(10)
        data['maxFilesSize'] = random_int(5)
        data['maxDocuments'] = random_int(3)
        data['description'] = random_word(20)
        data['versioned'] = True
        data['agreementRequired'] = False
        data['numbered'] = True
        data['visibleInProject'] = False
        data['documentGroups'] = []

        response = self.sender.put('documenttypes/' + str(new_id), data)
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)

        response = self.sender.get('documenttypes/' + str(new_id))
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documenttypes')
    @allure.story('DELETE  /documenttypes/:id')
    @pytest.mark.run(after='test_documenttypes_put')
    def test_documenttypes_delete(self):

        new_id = TestDocumentTypes.id

        response = self.sender.delete('documenttypes/' + str(new_id))
        assert response.status_code == 200

        response = self.sender.get('documenttypes')
        response_json = response.json()
        assert response.status_code == 200
        assert_key_not_in_json_array(response_json, 'id', new_id)






