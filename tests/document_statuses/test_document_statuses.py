# coding=utf-8
__author__ = 'slugovskoy'
from sender.request_sender import Sender
from helper.validator import *
from helper.generator import *
import pytest
import allure
import json
import os
import pprint
import collections

class TestDocumentStatuses:

    @classmethod
    def setup_class(cls):

        cls.sender = Sender()
        cls.sender.authorize()
        cls.response = cls.sender.get('documentstatuses')
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documentstatuses')
    @allure.story('GET     /documentstatuses')
    def test_documents_status_get(self):

        assert self.response.status_code == 200
        validate_json(self.json_dump, self.schema)

        TestDocumentStatuses.id = self.json_dump[0]['id']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documentstatuses')
    @allure.story('GET     /documentstatuses:id')
    @pytest.mark.run(after='test_documents_status_get')
    def test_documents_status_get_id(self):

        new_id = TestDocumentStatuses.id
        response = self.sender.get('documentstatuses/' + str(new_id))
        response_json = response.json()
        assert response.status_code == 200

        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documentstatuses')
    @allure.story('POST    /documentstatuses')
    @pytest.mark.run(after='test_documents_status_get_id')
    def test_documents_status_post(self):

        documenttypes_items = self.sender.get('documenttypes').json()
        documenttypes = list()
        for item in documenttypes_items:
            documenttypes.append(item['id'])

        data = dict()
        data['shortName'] = random_word(10)
        data['name'] = random_word(5)
        data['description'] = random_word(22)
        data['documentTypes'] = documenttypes

        response = self.sender.post('documentstatuses', data)
        assert response.status_code == 200
        response_json = response.json()

        TestDocumentStatuses.new_id = response_json['id']

        response = self.sender.get('documentstatuses/' + str(TestDocumentStatuses.new_id))
        assert response.status_code == 200
        response_json = response.json()

        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documentstatuses')
    @allure.story('PUT     /documentstatuses/:id')
    @pytest.mark.run(after='test_documents_status_post')
    def test_documents_status_put(self):

        documenttypes_items = self.sender.get('documenttypes').json()
        documenttypes = list()
        for item in documenttypes_items:
            documenttypes.append(item['id'])

        new_id = TestDocumentStatuses.new_id
        data = dict()
        data['shortName'] = random_word(10)
        data['name'] = random_word(5)
        data['description'] = random_word(22)
        data['documentTypes'] = documenttypes

        response = self.sender.put('documentstatuses/' + str(new_id), data)
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)

        response = self.sender.get('documentstatuses/' + str(new_id))
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/documentstatuses')
    @allure.story('DELETE  /documentstatuses/:id')
    @pytest.mark.run(after='test_documents_status_put')
    def test_documents_status_delete(self):

        new_id = TestDocumentStatuses.new_id

        response = self.sender.delete('documentstatuses/' + str(new_id))
        assert response.status_code == 200

        response = self.sender.get('documentstatuses')
        response_json = response.json()
        assert response.status_code == 200
        assert_key_not_in_json_array(response_json, 'id', new_id)











