# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.organization import Organization

class TestOrganizations:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('POST     /organizations')
    @pytest.mark.first
    def test_organizations_post(self):

        organization = Organization()

        response = self.rest.post_organizations(organization.json())
        assert response.status_code == 200

        response_json = response.json()
        TestOrganizations.id = response_json['id']
        organization.id = TestOrganizations.id

        assert organization.name == response_json['name']

        check_all_object_fields(organization, response)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('PUT     /organizations/:id ')
    @pytest.mark.run(after='test_organizations_post')
    def test_organizations_put(self):

        organization = Organization()

        response = self.rest.put_organizations(organization.json(), TestOrganizations.id)
        assert response.status_code == 200
        check_all_object_fields(organization, response)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('GET     /organizations/:id')
    @pytest.mark.run(after='test_organizations_put')
    def test_organizations_get_id(self):

        response = self.rest.get_organizations_id(TestOrganizations.id)
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('GET     /organizations')
    @pytest.mark.run(after='test_organizations_put')
    def test_organizations_get(self):

        response = self.rest.get_organizations()
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('GET     /organizations')
    @pytest.mark.run(after='test_organizations_put')
    def test_organizations_get_roots(self):

        response = self.rest.get_organizations_roots()
        assert response.status_code == 200



    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('GET     /organizations/:id/parent')
    @pytest.mark.run(after='test_organizations_put')
    def test_organizations_get_id_parent(self):

        response = self.rest.get_organizations_id_parent('321')
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)


    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('GET     /organizations/:id/persons')
    @pytest.mark.run(after='test_organizations_put')
    def test_organizations_get_id_persons(self):
        pytest.skip('нет организации с person')
        # хардкод id т к в нельзя добавить такую организацию  - только через базу
        response = self.rest.get_organizations_id_persons('835')
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('GET     /organizations?mainContractor=true')
    @pytest.mark.run(after='test_organizations_put')
    def test_organizations_maincontractor(self):
        response = self.rest.get_organizations_maincontractor()
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('GET     /organizations?externalcontrol=true')
    @pytest.mark.run(after='test_organizations_put')
    def test_organizations_externalcontrol(self):
        response = self.rest.get_organizations_externalcontrol()
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('GET     /organizations?maincustomer=true')
    @pytest.mark.run(after='test_organizations_put')
    def test_organizations_maincustomer(self):
        response = self.rest.get_organizations_maincustomer()
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizations')
    @allure.story('DELETE  /organizations/:id')
    @pytest.mark.last
    def test_organizations_delete(self):

        response = self.rest.delete_organizations_id(TestOrganizations.id)
        assert response.status_code == 200

        response = self.rest.get_organizations()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', TestOrganizations.id)













