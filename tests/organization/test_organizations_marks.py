# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.organization import OrganizationMark, Organization


class TestOrganizationsMarks:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema_marks.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id_marks.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizationmarks')
    @allure.story('POST     /organizationmarks')
    @pytest.mark.first
    def test_organizations_marks_post(self):
        mark = OrganizationMark()

        response = self.rest.post_organizations_marks(mark.json())
        assert response.status_code == 200

        response_json = response.json()
        TestOrganizationsMarks.id = response_json['id']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizationmarks')
    @allure.story('PUT     /organizationmarks')
    @pytest.mark.second
    def test_organizations_marks_put(self):
        mark = OrganizationMark()

        response = self.rest.put_organizations_marks(mark.json(), TestOrganizationsMarks.id)
        assert response.status_code == 200

        response_json = response.json()
        TestOrganizationsMarks.id = response_json['id']

        response = self.rest.get_organizations_marks_id(TestOrganizationsMarks.id).json()
        assert response['name'] == mark.name
        assert response['shortName'] == mark.shortName
        assert response['description'] == mark.description

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizationmarks')
    @allure.story('GET     /organizationmarks')
    @pytest.mark.second
    def test_organizations_marks_get(self):
        response = self.rest.get_organizations_marks()
        assert response.status_code == 200
        validate_json(response.json(), self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizationmarks')
    @allure.story('GET     /organizationmarks:id')
    @pytest.mark.run(after='test_organizations_marks_put')
    def test_organizations_marks_get_id(self):
        response = self.rest.get_organizations_marks_id(TestOrganizationsMarks.id)
        assert response.status_code == 200
        validate_json(response.json(), self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizationmarks')
    @allure.story('GET     /organizations/filter')
    @pytest.mark.run(after='test_organizations_marks_put')
    def test_organizations_marks_filter(self):
        organization = Organization()
        organization_mark = OrganizationMark(id=TestOrganizationsMarks.id)
        organization.organizationMarks = [organization_mark]

        # post organization with mark
        response = self.rest.post_organizations(organization.json())
        organization.id = response.json()['id']
        assert response.status_code == 200

        # get organization with mark
        response = self.rest.get_organizations_filter(filters='hasMarks=' + str(TestOrganizationsMarks.id))
        assert response.status_code == 200
        assert response.json()['items'][0]['name'] == organization.name
        assert len(response.json()['items']) == 1

        response = self.rest.get_organizations_id(organization.id)

        assert response.json()['organizationMarks'][0]['id'] == TestOrganizationsMarks.id

        # delete organization with mark
        response = self.rest.delete_organizations_id(organization.id)
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizationmarks')
    @allure.story('GET     /organizations/filter')
    @pytest.mark.run(after='test_organizations_marks_put')
    def test_organizations_telegraph_name_filter(self):
        organization = Organization()
        organization.telegraphName = random_word(10) + 'test telegraphName'
        response = self.rest.post_organizations(organization.json())
        assert response.status_code == 200

        response = self.rest.get_organizations_q(filters=organization.telegraphName)
        assert response.status_code == 200
        assert len(response.json()) == 1
        assert response.json()[0]['name'] == organization.name

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizationmarks')
    @allure.story('GET     /organizations/filter')
    @pytest.mark.run(after='test_organizations_marks_put')
    def test_organizations_default_marks(self):
        response = self.rest.get_organizations_maincustomer()
        assert len(response.json()) > 100

        response = self.rest.get_organizations_mainexecutor()
        assert len(response.json()) > 800

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizationmarks/filter?except: String ?= ""')
    @allure.story('GET     /organizations/filter?except: String ?= ""')
    @pytest.mark.run(after='test_organizations_default_marks')
    def test_organizations_except(self):
        organization = Organization()
        organization_id = self.rest.post_organizations(organization.json()).json()['id']

        response = self.rest.get_organizations_filter('except=' + str(organization_id))
        assert response.status_code == 200
        assert any(item['id'] != organization_id for item in response.json()['items'])

        self.rest.delete_organizations_id(organization_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/organizationmarks')
    @allure.story('DELETE     /organizationmarks:id')
    @pytest.mark.last
    def test_organizations_marks_delete(self):
        response = self.rest.delete_organizations_marks_id(TestOrganizationsMarks.id)
        assert response.status_code == 200

        response = self.rest.get_organizations_marks_id(TestOrganizationsMarks.id)
        assert response.status_code == 400






