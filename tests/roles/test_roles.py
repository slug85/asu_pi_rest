# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
from classes.role import *
from classes.role_access import *
from classes.user import *
import pytest
import allure
import json
import os
import pprint
import collections
import time


class TestRoles:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_roles()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/roles')
    @allure.story('POST     /roles')
    @pytest.mark.first
    def test_roles_post(self):

        data = Role().json()
        response = self.rest.post_roles(data)
        assert response.status_code == 200
        response_json = response.json()
        TestRoles.id = response_json['id']
        print TestRoles.id

        response = self.rest.get_roles_id(TestRoles.id)
        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

        assert response.json()['roleAccesses'][0]['access'] == 'ARRANGEMENT_PLAN'

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/roles')
    @allure.story('PUT     /roles')
    @pytest.mark.run(after='test_roles_post')
    def test_roles_put(self):

        role = Role()
        response = self.rest.put_roles(data=role.json(), id=TestRoles.id)
        assert response.status_code == 200
        assert response.json()['roleAccesses'][0]['type'] == 'CREATE'

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/roles')
    @allure.story('GET     /roles')
    @pytest.mark.run(after='test_roles_put')
    def test_roles_get(self):

        assert self.response.status_code == 200
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/roles')
    @allure.story('GET     /roles/:id')
    @pytest.mark.run(after='test_roles_put')
    def test_roles_get_id(self):

        response = self.rest.get_roles_id(TestRoles.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/roles')
    @allure.story('PUT     /roles/:id')
    @pytest.mark.run(after='test_roles_put')
    def test_roles_put_access(self):

        role = Role()
        role.roleAccesses = [RoleAccess(typ='DELETE')]
        response = self.rest.put_roles(role.json(), TestRoles.id)
        assert response.status_code == 200
        assert len(response.json()['roleAccesses']) > 0

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/roles')
    @allure.story('PUT     /persons')
    @pytest.mark.run(after='test_roles_put_access')
    def test_roles_put_document_add_access(self):

        # добавление персоны к роли, утверждающей докумеенты

        # создать пользователя
        person = Person()
        response = self.rest.post_users(person.json())
        person_id = response.json()['id']

        # добавить ему права
        response = self.rest.put_users(person.json(), person_id)
        assert response.status_code == 200
        assert response.json()['documentTypeStatuses'][0]['canAgree'] is False

        person = Person()
        person.documentTypeStatuses = [DocumentTypeStatuses(can_agree=True, can_approve=True, can_observe=True)]
        response = self.rest.put_users(person.json(), person_id)

        # проверка сохранения прав на документ с типом 1
        assert response.status_code == 200
        assert response.json()['documentTypeStatuses'][0]['canAgree'] is True
        assert response.json()['documentTypeStatuses'][0]['canApprove'] is True
        assert response.json()['documentTypeStatuses'][0]['canObserve'] is True

        # проверка что можно утверждать с этим юзером
        response = self.rest.get_persons_can_approve(doc_type=1)
        assert response.status_code == 200

        assert any(item['lastName'] == person.lastName for item in response.json()) is True


    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/roles')
    @allure.story('PUT     /persons')
    @pytest.mark.run(after='test_roles_put_access')
    def test_roles_put_document_remove_access(self):

        # добавление персоны к роли, утверждающей докумеенты

        # создать пользователя
        person = Person()
        response = self.rest.post_users(person.json())
        person_id = response.json()['id']

        # добавить ему права
        response = self.rest.put_users(person.json(), person_id)
        assert response.status_code == 200
        assert response.json()['documentTypeStatuses'][0]['canAgree'] is False

        person = Person()
        person.documentTypeStatuses = [DocumentTypeStatuses(can_agree=True, can_approve=True, can_observe=True)]
        response = self.rest.put_users(person.json(), person_id)

        # проверка сохранения прав на документ с типом 1
        assert response.status_code == 200
        assert response.json()['documentTypeStatuses'][0]['canAgree'] is True
        assert response.json()['documentTypeStatuses'][0]['canApprove'] is True
        assert response.json()['documentTypeStatuses'][0]['canObserve'] is True

        new_person = person
        new_person.documentTypeStatuses = [DocumentTypeStatuses(can_agree=False, can_approve=False, can_observe=False)]
        response = self.rest.put_users(new_person.json(), person_id)
        assert response.status_code == 200

        # проверка что нельзя утверждать с этим юзером
        response = self.rest.get_persons_can_approve(doc_type=1)
        assert response.status_code == 200

        assert any(item['lastName'] == person.lastName for item in response.json()) is False

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/roles')
    @allure.story('DELETE  /roles/:id')
    @pytest.mark.last
    def test_roles_delete(self):
        response = self.rest.delete_roles_id(TestRoles.id)
        assert response.status_code == 200

        response = self.rest.get_roles()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', TestRoles.id)
