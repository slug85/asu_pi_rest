# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
from classes.user import Person
from classes.role import Role
import pytest
import allure
import json
import os
import pprint
import collections
import time


class TestAccesses:
    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema_accesses.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/accesses')
    @allure.story('GET     /accesses')
    @pytest.mark.first
    def test_accesses_get(self):

        response = self.rest.get_accesses()
        assert response.status_code == 200
        json_dump = response.json()
        TestAccesses.accesses = json_dump

        validate_json(json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/accesses')
    @allure.story('POST     /roles with access')
    def test_roles_post_with_access(self):

        global admin_access, delete_access
        data = dict()
        data['description'] = random_word(25)
        data['name'] = random_word(7)
        data['shortName'] = random_word(3)

        admin_label = 'Администраторы'.decode('utf-8')

        for access in TestAccesses.accesses:
            for id, label in access.iteritems():
                if label == admin_label: admin_access = access

        # get access types
        types = self.rest.get_access_types().json()
        delete_label = 'Удаление'.decode('utf-8')

        for access_type in types:
            for id, label in access_type.iteritems():
                if label == delete_label:
                    delete_access = access_type

        data['roleAccesses'] = [{"access": "ADMIN", "type": "DELETE"}, {}]

        response = self.rest.post_roles(data)

        TestAccesses.role_id = response.json()['id']

        assert response.status_code == 200

        assert response.json()['roleAccesses'][0]['access'] == 'ADMIN'
        assert response.json()['roleAccesses'][0]['accessReadable'] == admin_label
        assert response.json()['roleAccesses'][0]['type'] == 'DELETE'
        assert response.json()['roleAccesses'][0]['value'] == 'ADMIN.DELETE'

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/accesses')
    @allure.story('POST     /user with role access')
    @pytest.mark.run(after='test_roles_post_with_access')
    def test_roles_post_user_with_role(self):

        person = Person()
        role = Role()
        role.id = TestAccesses.role_id
        person.user.roles = [{"id": role.id}]
        response = self.rest.post_users(person.json()).json()

        assert len(response['user']['permissions']) > 0
        for permission in response['user']['permissions']:
            assert 'ADMIN.DELETE' in permission







