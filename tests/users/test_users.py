# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.user import Person

class TestUsers:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_users()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())
        cls.schema_post = json.loads(open(cls.current_dir + "/schema_post.json", 'r').read())
        cls.skip_all = False

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/users')
    @allure.story('POST     /users')
    @pytest.mark.first
    def test_users_post(self):

        user = Person()

        TestUsers.organization_id = self.rest.get_organizations().json()[0]['id']
        user.organization = {"id": TestUsers.organization_id}

        response = self.rest.post_users(user.json())
        assert response.status_code == 200
        response_json = response.json()

        validate_json(response_json, self.schema_post)
        TestUsers.id = response_json['user']['id']
        TestUsers.person_id = response_json['id']

        assert response.json()['email'] == user.email

        response = self.rest.get_users_id(TestUsers.person_id)
        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/users')
    @allure.story('PUT     /users')
    @pytest.mark.run(after='test_users_post')
    def test_users_put(self):

        user = Person()
        response = self.rest.put_users(data=user.json(), id=TestUsers.person_id)

        assert response.status_code == 200
        assert response.json()['email'] == user.email

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/users')
    @allure.story('GET     /users')
    @pytest.mark.run(after='test_users_put')
    def test_users_get(self):

        assert self.response.status_code == 200
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/users')
    @allure.story('GET     /users/:id')
    @pytest.mark.run(after='test_users_put')
    def test_users_get_id(self):

        response = self.rest.get_users_id(TestUsers.person_id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/users')
    @allure.story('GET     /users/:id/projects')
    @pytest.mark.run(after='test_users_put')
    def test_users_get_id_projects(self):

        response = self.rest.get_users_id_projects(TestUsers.id)
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/users')
    @allure.story('DELETE  /users/:id')
    @pytest.mark.last
    def test_users_delete(self):

        response = self.rest.delete_users_id(TestUsers.person_id)
        assert response.status_code == 200

        response = self.rest.get_users()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', TestUsers.id)
