# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.project import *


class TestPersonAttributes:
    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_personattributes()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/personattributes')
    @allure.story('POST     /personattributes')
    @pytest.mark.first
    def test_personattributes_post(self):
        person_attribute = PersonAttribute()

        response = self.rest.post_personattributes(person_attribute.json())
        assert response.status_code == 200
        response_json = response.json()
        TestPersonAttributes.id = response_json['id']

        response = self.rest.get_personattributes_id(TestPersonAttributes.id)
        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/personattributes')
    @allure.story('PUT     /personattributes')
    @pytest.mark.run(after='test_personattributes_post')
    def test_personattributes_put(self):
        plan_pi = PlanPi()

        response = self.rest.put_personattributes(data=plan_pi.json(), id=TestPersonAttributes.id)
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/personattributes')
    @allure.story('GET     /personattributes')
    @pytest.mark.run(after='test_personattributes_put')
    def test_personattributes_get(self):
        response = self.rest.get_personattributes()
        assert response.status_code == 200
        validate_json(response.json(), self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/personattributes')
    @allure.story('GET     /personattributes/:id')
    @pytest.mark.run(after='test_personattributes_put')
    def test_personattributes_get_id(self):
        response = self.rest.get_personattributes_id(TestPersonAttributes.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/personattributes')
    @allure.story('DELETE  /personattributes/:id')
    @pytest.mark.run(after='test_personattributes_get')
    def test_personattributes_delete(self):
        response = self.rest.delete_personattributes_id(TestPersonAttributes.id)
        assert response.status_code == 200

        response = self.rest.get_personattributes()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', TestPersonAttributes.id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/personattributes')
    @allure.story('GET  /personattributes')
    @pytest.mark.last
    def test_personattributes_default(self):
        response = self.rest.get_personattributes()
        person_attributes = list()
        for item in response.json():
            person_attributes.append(item['name'])

        assert 'Куратор проектов'.decode('utf-8') in person_attributes
        assert 'Ответственный руководитель от ИТ'.decode('utf-8') in person_attributes
        assert 'Топ-менеджер ОАО «РЖД», выдающий контрольные поручения'.decode('utf-8') in person_attributes
        assert 'Утверждает КП проектов'.decode('utf-8') in person_attributes
        assert 'Утверждает основания проектов'.decode('utf-8') in person_attributes
        assert 'Ответственный за виды работ планов ПИ'.decode('utf-8') in person_attributes
        assert 'Координатор темы'.decode('utf-8') in person_attributes