# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
from classes.state_settings import StateSetting, StateSettingsType
from classes.statuses import States

class TestStateSettings:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())

    # роуты для теста
    routes = [StateSettingsType.milestone.value,
              StateSettingsType.arrangement_plan_stage.value,
              StateSettingsType.project.value,
              StateSettingsType.arrangement.value,
              StateSettingsType.arrangement_plan.value,
              StateSettingsType.arrangement_plan_stage.value
              ]

    @pytest.mark.parametrize("route", routes)
    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/statesettings')
    @allure.story('POST     /statesettings')
    @pytest.mark.first
    def test_state_settings_post(self, route):

        state_setting = StateSetting()

        # get normal state
        response = self.rest.get_state_settings(route=route)
        assert response.status_code == 200
        assert any(item['state'] == States.normal.value for item in response.json()) is True

        for item in response.json():
            if item['state'] == States.normal.value:
                state_setting.state = item['state']
                TestStateSettings.id = str(item['id'])
                state_setting.thresholdValue = item['thresholdValue'] + 1

                # delete normal state
                response = self.rest.delete_state_settings(route=route, id=TestStateSettings.id)
                assert response.status_code == 200

                # check deleted
                response = self.rest.get_state_settings_id(route=route, id=TestStateSettings.id)
                assert response.status_code == 400

                # post new normal state
                response = self.rest.post_state_settings(data=state_setting.json(), route=route)
                TestStateSettings.id = response.json()['id']
                assert response.status_code == 200

                # check state saved
                response = self.rest.get_state_settings_id(route=route, id=str(TestStateSettings.id))
                assert response.status_code == 200

                # put
                state_setting = StateSetting()
                state_setting.color = '#5cb85c'
                state_setting.thresholdValue = 80
                response = self.rest.put_state_settings(data=state_setting.json(), route=route, id=str(TestStateSettings.id))
                assert response.status_code == 200
                assert response.json()['color'] == state_setting.color
                assert response.json()['thresholdValue'] == state_setting.thresholdValue

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/statesettings')
    @allure.story('GET     /statesettings')
    @pytest.mark.parametrize("route", routes)
    @pytest.mark.second
    def test_state_settings_get(self, route):

        response = self.rest.get_state_settings(route=route)
        assert response.status_code == 200
        validate_json(response.json(), schema=self.schema)