# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.project import Project
from sender.request_sender import Sender
from classes.calendar_plan import CalendarPlanStage, CalendarPlan, Milestone

class TestMilestones:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.sender = Sender()
        cls.rest.auth()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/milestones')
    @allure.story('POST     /milestones')
    @pytest.mark.first
    def test_milestones_post(self):

        project = Project()
        TestMilestones.project_id = self.rest.post_projects(project.json()).json()['id']

        # этап КП
        calendar_plan_stage = CalendarPlanStage(confirm=True)

        # КП
        data_kp = CalendarPlan(project_id=TestMilestones.project_id, calendar_plan_stages=[calendar_plan_stage.json()]).json()
        response = self.sender.put('projects/' + str(TestMilestones.project_id) + '/calendarPlan', data_kp)
        assert response.status_code == 200

        # получаю id созданного этапа
        calendar_plan_stage.id = response.json()['calendarPlanStages'][0]['id']

        # Веха
        milestone = Milestone(project_id=TestMilestones.project_id,
                              confirm=True,
                              calendar_plan_stage=calendar_plan_stage)

        data = milestone.json()
        TestMilestones.milestone = milestone
        response = self.rest.post_milestones(data)
        assert response.status_code == 200

        response_json = response.json()
        TestMilestones.id = response_json['id']


    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/milestones')
    @allure.story('PUT     /milestones/:id ')
    @pytest.mark.run(after='test_milestones_post')
    def test_milestones_put(self):

        milestone = TestMilestones.milestone
        milestone.name = random_word(20)
        TestMilestones.milestone = milestone

        response = self.rest.put_milestones(milestone.json(), TestMilestones.id)
        assert response.status_code == 200
        response_json = response.json()
        assert milestone.name == response_json['name']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/milestones')
    @allure.story('GET     /milestones/:id')
    @pytest.mark.run(after='test_milestones_put')
    def test_milestones_get_id(self):

        response = self.rest.get_milestones_id(TestMilestones.id)
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/milestones')
    @allure.story('GET     /milestones')
    @pytest.mark.run(after='test_milestones_put')
    def test_milestones_get(self):

        response = self.rest.get_milestones()
        assert response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/milestones')
    @allure.story('GET     /workPlan filters')
    @pytest.mark.run(after='test_milestones_put')
    def test_milestones_filter(self):

        # меняю этап так чтобы веха вышла из этапа

        # этап КП
        calendar_plan_stage = CalendarPlanStage(end_date=get_unix_time_in_past(2))

        # КП
        data_kp = CalendarPlan(project_id=TestMilestones.project_id, calendar_plan_stages=[calendar_plan_stage.json()]).json()
        response = self.sender.put('projects/' + str(TestMilestones.project_id) + '/calendarPlan', data_kp)
        assert response.status_code == 200

        response = self.rest.get_projects_id_workPlan(TestMilestones.project_id, filters='?behindStages=false&lostStages=true&finished=false')
        assert response.status_code == 200

        # проверка что в роуте workPlan присутствует веха при фильтре ?behindStages=false&lostStages=true&finished=false
        stages_and_milestones = response.json()['byStages']['stagesAndMilestones']
        assert len(stages_and_milestones) > 0
        assert any(item['name'] == TestMilestones.milestone.name for item in stages_and_milestones) is True

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/milestones')
    @allure.story('DELETE  /milestones/:id')
    @pytest.mark.last
    def test_milestones_delete(self):

        response = self.rest.delete_milestones_id(TestMilestones.id)
        assert response.status_code == 200

        response = self.rest.get_milestones()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', TestMilestones.id)













