# coding=utf-8
__author__ = 'slugovskoy'
from sender.request_sender import Sender
from helper.validator import *
from helper.generator import *
import pytest
import allure
import json
import os
import pprint
import collections

class TestUpload:

    @classmethod
    def setup_class(cls):

        cls.sender = Sender()
        cls.sender.authorize()
        cls.current_dir = os.path.dirname(__file__)
        cls.upload_file = open(cls.current_dir + "/upload_file.txt", 'r')

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/upload')
    @allure.story('POST    /upload')
    def test_upload_post(self):

        text = self.upload_file.read()
        response = self.sender.post_upload(text)
        assert response.status_code == 200








