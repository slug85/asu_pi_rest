# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time

class TestSystemObjects:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_systemobjects()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/systemobjects')
    @allure.story('GET     /systemobjects')
    def test_systemobjects_get(self):


        assert self.response.status_code == 200
        TestSystemObjects.id = self.json_dump[0]['id']
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/systemobjects')
    @allure.story('GET     /systemobjects/:id')
    @pytest.mark.run(after='test_systemobjects_get')
    def test_systemobjects_get_id(self):

        response = self.rest.get_systemobjects_id(TestSystemObjects.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)
