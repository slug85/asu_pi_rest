# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.project import *

class TestExecutorSelection:

    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/executorSelection')
    @allure.story('POST     /executorSelection')
    @pytest.mark.first
    def test_executor_selection_post(self):

        executor_selection = ExecutorSelection()

        response = self.rest.post_executor_selection(executor_selection.json())
        assert response.status_code == 200
        response_json = response.json()
        TestExecutorSelection.id = response_json['id']

        response = self.rest.get_executor_selection()
        assert any(m['id'] == TestExecutorSelection.id for m in response.json()) is True

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/executorSelection')
    @allure.story('GET     /executorSelection')
    @pytest.mark.run(after='test_executor_selection_post')
    def test_executor_selection_get(self):

        response = self.rest.get_executor_selection_id(TestExecutorSelection.id)
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/executorSelection')
    @allure.story('PUT     /executorSelection')
    @pytest.mark.run(after='test_executor_selection_get')
    def test_executor_selection_put(self):

        executor_selection = ExecutorSelection()
        response = self.rest.put_executor_selection(TestExecutorSelection.id, executor_selection.json())
        assert response.status_code == 200

        assert executor_selection.name == response.json()['name']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/executorSelection')
    @allure.story('DELETE     /executorSelection')
    @pytest.mark.run(after='test_executor_selection_put')
    def test_executor_delete(self):

        response = self.rest.delete_executor_selection(TestExecutorSelection.id)
        assert response.status_code == 200

        response = self.rest.get_executor_selection()
        for item in response.json():
            assert TestExecutorSelection.id != item['id']