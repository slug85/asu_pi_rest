# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time


class TestAsu:
    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_asu()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/asu')
    @allure.story('POST     /asu')
    @pytest.mark.first
    def test_asu_post(self):

        data = dict()
        data['description'] = random_word(25)
        data['name'] = random_word(7)
        data['code'] = random_code()
        data['shortName'] = random_word(3)

        response = self.rest.post_asu(data)
        assert response.status_code == 200
        response_json = response.json()
        TestAsu.id = response_json['id']
        response = self.rest.get_asu_id(TestAsu.id)
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/asu')
    @allure.story('PUT     /asu')
    @pytest.mark.run(after='test_asu_post')
    def test_asu_put(self):

        data = dict()
        data['description'] = random_word(25)
        data['name'] = random_word(7)
        data['shortName'] = random_word(3)
        response = self.rest.put_asu(data=data, id=TestAsu.id)
        assert response.status_code == 200

        response = self.rest.get_asu_id(TestAsu.id)
        assert response.status_code == 200
        assert_node_in_json(response.json(), keys='name', data=data)


    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/asu')
    @allure.story('GET     /asu')
    @pytest.mark.run(after='test_asu_put')
    def test_asu_get(self):

        assert self.response.status_code == 200
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/asu')
    @allure.story('GET     /asu/:id')
    @pytest.mark.run(after='test_asu_put')
    def test_asu_get_id(self):

        response = self.rest.get_asu_id(TestAsu.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/asu')
    @allure.story('GET     /asu/filter ')
    @pytest.mark.run(after='test_asu_put')
    def test_asu_get_filter(self):

        i = 0
        # берутся все имена АСУ и подставляются в поиск в цикле
        asu_list = self.rest.get_asu()
        for item in asu_list.json():
            i += 1
            # для каждого 20-го АСУ, т.к. иначе тест идет слишком долго
            if i % 20 == 0:
                name = item['name'].encode('utf-8')
                code = item['code']
                short_name = item['shortName'].encode('utf-8')
                response = self.rest.get_asu_filter(q=name)
                response_code_search = self.rest.get_asu_filter(q=code)
                response_short_name_search = self.rest.get_asu_filter(q=short_name)
                assert len(response.json()) != 0
                assert len(response_code_search.json()) != 0
                assert len(response_short_name_search.json()) != 0

                for result in response.json():
                    # проверка что имя из выдачи содержится в тексте запроса
                    words = name.decode('utf-8').split(' ', 1)
                    assert any(word.lower() in result['name'].lower() for word in words)

                for result in response_short_name_search.json():
                    # проверка что краткое название из выдачи содержится в тексте запроса
                    words = short_name.decode('utf-8').split(' ', 1)
                    assert (any(word.lower() in result['shortName'].lower() for word in words) or any(word.lower() in result['name'].lower() for word in words))





    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/asu')
    @allure.story('DELETE  /asu/:id')
    @pytest.mark.last
    def test_asu_delete(self):

        response = self.rest.delete_asu_id(TestAsu.id)
        assert response.status_code == 200

        response = self.rest.get_asu()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', TestAsu.id)