# coding=utf-8
from sender.request_sender import Sender
from helper.validator import validate_json
from helper.generator import random_word
import pytest
import allure
import json
import os
import pprint
import collections


class TestProjectCalendarPlanStages:
    @classmethod
    def setup_class(cls):
        cls.sender = Sender()
        cls.sender.authorize()
        cls.response = cls.sender.get('projectCalendarPlanStages/1')
        cls.json_dump = cls.response.json()


    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectCalendarPlanStages')
    @allure.story('GET     /projectCalendarPlanStages ')
    def test_project_calendar_plan_stages_get(self):
        if len(self.json_dump) == 0:
            pytest.skip('апи ничего не отдает')
        assert self.response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @allure.feature('/projectCalendarPlanStages')
    @allure.story('GET     /projectCalendarPlanStages/:id ')
    def test_project_calendar_plan_stages_get_id(self):
        if len(self.json_dump) == 0:
            pytest.skip('апи ничего не отдает')
