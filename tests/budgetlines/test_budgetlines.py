# coding=utf-8
from sender.request_sender import Sender
from helper.validator import validate_json
from helper.generator import random_word
import pytest
import allure
import json
import os
import pprint
import collections



class TestBudgetLines:

    @classmethod
    def setup_class(cls):
        cls.sender = Sender()
        cls.sender.authorize()
        cls.response = cls.sender.get('budgetlines?mode=full')
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/budgetlines')
    @allure.story('GET     /budgetlines')
    def test_budgetlines_get(self):
        assert self.response.status_code is 200
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @allure.feature('/budgetlines')
    @allure.story('GET     /budgetlines/:id ')
    def test_budgetlines_get_id(self):
        budgetlines_id = str(self.json_dump[0]['id'])
        json_dump = self.sender.get('budgetlines/' + budgetlines_id + '?mode=full').json()
        validate_json(json_dump, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @allure.feature('/budgetlines')
    @allure.story('PUT     /budgetlines/:id ')
    def test_budgetlines_put(self):

        # получение первого объекта из json
        budgetlines_id = self.json_dump[0]['id']
        financial_id = self.json_dump[0]['financialSource']['id']

        data = collections.OrderedDict()

        # рандомные данные для PUT запроса
        data['financialSource'] = financial_id
        data['name'] = random_word(10)
        data['shortName'] = random_word(5)
        data['description'] = random_word(15)

        response = self.sender.put('budgetlines/' + str(budgetlines_id), data)
        assert response.status_code is 200

        # проверка что в ответе новые данные
        response_json = response.json()
        assert response_json['id'] == budgetlines_id
        assert response_json['description'] == data['description']
        assert response_json['name'] == data['name']
        assert response_json['shortName'] == data['shortName']

        # проверка что данные сохранились
        response = self.sender.get('budgetlines/' + str(budgetlines_id))
        response_json = response.json()
        assert response_json['id'] == budgetlines_id
        assert response_json['description'] == data['description']
        assert response_json['name'] == data['name']
        assert response_json['shortName'] == data['shortName']

        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @allure.feature('/budgetlines')
    @allure.story('POST    /budgetlines ')
    @pytest.mark.run(after='test_budgetlines_put')
    def test_budgetlines_post(self):

        budgetlines_id = self.json_dump[0]['id']
        financial_id = self.json_dump[0]['financialSource']['id']

        data = collections.OrderedDict()

        data['name'] = random_word(10)
        data['shortName'] = random_word(5)
        data['description'] = random_word(15)
        data['id'] = budgetlines_id
        data['financialSource'] = financial_id

        response = self.sender.post('budgetlines', data)
        assert response.status_code is 200
        response_json = response.json()

        TestBudgetLines.new_id = response_json['id']  # id нового объекта для последующего удаления через DELETE запрос
        assert response_json['id'] != budgetlines_id                # проверка что объект записался с новым id
        assert response_json['description'] == data['description']
        assert response_json['name'] == data['name']
        assert response_json['shortName'] == data['shortName']

        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @allure.feature('/budgetlines')
    @allure.story('DELETE  /budgetlines/:id ')
    @pytest.mark.run(after='test_budgetlines_post')
    def test_budgetlines_delete(self):

        # удаление полученного ранее id
        del_id = TestBudgetLines.new_id
        response = self.sender.delete('budgetlines/' + str(del_id))
        assert response.status_code == 200

        # просмотр всех id в выдаче
        response = self.sender.get('budgetlines')
        response_json = response.json()

        # проверка что отсутствует удаленный id
        assert any(item['id'] for item in response_json) != del_id









