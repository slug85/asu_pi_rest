# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
from classes.lifecycle_stage import LifeCycleStage
from classes.project import Project
import pytest
import allure
import json
import os
import pprint
import collections
import time


class TestProjectLifecycleStages:
    @classmethod
    def setup_class(cls):

        cls.rest = Rest()
        cls.rest.auth()
        cls.response = cls.rest.get_projectlifecyclestages()
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectlifecyclestages')
    @allure.story('POST     /projectlifecyclestages')
    @pytest.mark.first
    def test_projectlifecyclestages_post(self):

        stage = LifeCycleStage()

        response = self.rest.post_projectlifecyclestages(stage.json())
        assert response.status_code == 200
        response_json = response.json()
        assert response_json['name'] == stage.name
        assert response_json['number'] == stage.number
        assert response_json['shortName'] == stage.shortName

        TestProjectLifecycleStages.id = response_json['id']
        response = self.rest.get_projectlifecyclestages_id(TestProjectLifecycleStages.id)
        assert response.status_code == 200
        validate_json(response_json, self.schema_id)


    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectlifecyclestages')
    @allure.story('PUT     /projectlifecyclestages')
    @pytest.mark.run(after='test_projectlifecyclestages_post')
    def test_projectlifecyclestages_put(self):

        stage = LifeCycleStage()
        response = self.rest.put_projectlifecyclestages(data=stage.json(), id=TestProjectLifecycleStages.id)
        assert response.status_code == 200

        response_json = response.json()

        assert response_json['name'] == stage.name
        assert response_json['number'] == stage.number
        assert response_json['shortName'] == stage.shortName

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectlifecyclestages')
    @allure.story('GET     /projectlifecyclestages')
    @pytest.mark.run(after='test_projectlifecyclestages_put')
    def test_projectlifecyclestages_get(self):

        assert self.response.status_code == 200
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectlifecyclestages')
    @allure.story('GET     /projectlifecyclestages/:id')
    @pytest.mark.run(after='test_projectlifecyclestages_put')
    def test_projectlifecyclestages_get_id(self):

        response = self.rest.get_projectlifecyclestages_id(TestProjectLifecycleStages.id)
        response_json = response.json()

        assert response.status_code == 200
        validate_json(response_json, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectlifecyclestages')
    @allure.story('DELETE  /projectlifecyclestages/:id')
    @pytest.mark.last
    def test_projectlifecyclestages_delete(self):

        response = self.rest.delete_projectlifecyclestages_id(TestProjectLifecycleStages.id)
        assert response.status_code == 200

        response = self.rest.get_projectlifecyclestages()
        response_json = response.json()

        assert_key_not_in_json_array(response_json, 'id', TestProjectLifecycleStages.id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectlifecyclestages')
    @allure.story('GET  /projectlifecyclestages/:id')
    def test_projectlifecyclestages_limitations_get(self):

        stages = self.rest.get_projectlifecyclestages().json()
        for stage in stages:
            for status in stage['statuses']:
                name = status['projectStatus']['name']

                if name == 'На согласовании'.decode('utf-8'):
                    assert status['statusAutoSet'] is False

                elif name == 'Прекращен'.decode('utf-8'):
                    assert status['statusAutoSet'] is False

                elif name == 'Инициация проекта'.decode('utf-8'):
                    assert status['statusAutoSet'] is True

                elif name == 'Инициация проекта'.decode('utf-8'):
                    assert status['statusAutoSet'] is False

                elif name == 'Реализация проекта'.decode('utf-8'):
                    assert status['statusAutoSet'] is True

                elif name == 'Завершение проекта'.decode('utf-8'):
                    assert status['statusAutoSet'] is True

                elif name == 'В архиве'.decode('utf-8'):
                    assert status['statusAutoSet'] is False

    # пары ЖЦ и статусов проекта
    status_pairs = [(1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 8), (1, 9)]

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/projectlifecyclestages')
    @allure.story('POST  /project/')
    @pytest.mark.parametrize("lifecycle,status", status_pairs)
    @pytest.mark.last
    def test_project_lifecycle_stages_limitations_positive_test(self, lifecycle, status):

        project = Project(project_lifecycle_stage=lifecycle, project_status=status)

        response = self.rest.post_projects(project.json())
        assert response.status_code == 200





