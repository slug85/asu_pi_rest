# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest
import pytest
import allure
import json
import os
import pprint
import collections
import time
from classes.user import *
from classes.project import *

class TestLinkedObjects:

    @classmethod
    def setup_class(cls):
        cls.rest = Rest()
        cls.rest.auth()

    def test_linked_control_level(self):

        user = Person()
        response = self.rest.post_users(user.json())
        assert response.status_code == 200
        TestLinkedObjects.user_id = response.json()['id']

        data = Control(person_id=TestLinkedObjects.user_id).json()

        # post data
        response = self.rest.post_control(data)
        TestLinkedObjects.control_id = response.json()['id']
        assert response.status_code == 200
        response_json = response.json()
        assert response_json

        # добавление проекта
        project = Project()
        response = self.rest.post_projects(project.json())
        project_id = response.json()['id']
        TestLinkedObjects.project_id = project_id
        assert response.status_code == 200

        # добавление пма
        pm = ArrangementPlan(control_date=get_unix_time_in_future(2), project_id=project_id, control_level=TestLinkedObjects.control_id)
        response = self.rest.post_projects_id_arrangement_plan(pm.json(), project_id)
        plan_id = response.json()['id']
        TestLinkedObjects.plan_id = plan_id
        assert response.status_code == 200

        # удаление контроля
        response = self.rest.delete_control(TestLinkedObjects.control_id)
        assert response.status_code == 500

        response = self.rest.get_control_id(TestLinkedObjects.control_id)
        assert response.status_code == 200

        response = self.rest.get_projects_id_arrangement_plan(project_id)
        assert response.status_code == 200

