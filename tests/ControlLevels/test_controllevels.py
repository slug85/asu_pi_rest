# coding=utf-8
from sender.request_sender import Sender
from helper.validator import validate_json, assert_that_json_has_data, assert_key_not_in_json_array
from helper.generator import random_word, random_control_level
import pytest
import allure
import json
import os
import pprint
import collections
from classes.project import Control
from classes.user import User,Person
from helper.api_methods import Rest


class TestControlLevels:
    @classmethod
    def setup_class(cls):
        cls.sender = Sender()
        cls.sender.authorize()
        cls.rest = Rest()
        cls.response = cls.sender.get('controllevels')
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)
        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/controllevels')
    @allure.story('GET     /controllevels ')
    def test_controllevels_get(self):
        assert self.response.status_code is 200
        validate_json(self.json_dump, self.schema)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/controllevels')
    @allure.story('GET     /controllevels/:id ')
    def test_controllevels_get_id(self):
        controllevel_id = self.json_dump[0]['id']
        response = self.sender.get('controllevels/' + str(controllevel_id))
        assert response.status_code == 200
        json_dump = response.json()
        validate_json(json_dump, self.schema_id)

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/controllevels')
    @allure.story('POST    /controllevels')
    def test_controllevels_post(self):

        user = Person()
        response = self.rest.post_users(user.json())
        assert response.status_code == 200
        TestControlLevels.user_id = response.json()['id']

        data = Control(person_id=TestControlLevels.user_id).json()

        # post data
        response = self.sender.post('controllevels', data)
        assert response.status_code == 200
        response_json = response.json()
        assert response_json

        # новый id
        TestControlLevels.new_id = response_json['id']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/controllevels')
    @allure.story('PUT     /controllevels/:id')
    def test_controllevels_put(self):

        data = Control(person_id=TestControlLevels.user_id).json()
        new_id = TestControlLevels.new_id

        # put data
        response = self.sender.put('controllevels/' + str(new_id), data)
        assert response.status_code == 200
        response_json = response.json()
        assert response_json['name'] == data['name']

        new_id = response_json['id']

        response = self.sender.get('controllevels/' + str(new_id))
        assert response.status_code == 200

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/controllevels')
    @allure.story('DELETE  /controllevels/:id')
    @pytest.mark.run(after="test_controllevels_post")
    def test_controllevels_delete(self):

        # получение Id для удаления
        new_id = TestControlLevels.new_id
        response = self.sender.delete('controllevels/' + str(new_id))

        assert response.status_code == 200

        # проверка отсутствия id в выдаче
        response = self.sender.get('controllevels')
        response_json = response.json()
        assert_key_not_in_json_array(response_json, key='id', value=new_id)
















