# coding=utf-8
__author__ = 'slugovskoy'
from sender.request_sender import Sender
from helper.validator import *
from helper.generator import *
import pytest
import allure
import json
import os
import pprint
import collections
import time

class TestFiles:

    @classmethod
    def setup_class(cls):
        cls.sender = Sender()
        cls.sender.authorize()
        cls.response = cls.sender.get('files')
        cls.json_dump = cls.response.json()
        cls.current_dir = os.path.dirname(__file__)

        cls.schema = json.loads(open(cls.current_dir + "/schema.json", 'r').read())
        cls.schema_id = json.loads(open(cls.current_dir + "/schema_id.json", 'r').read())

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/files')
    @allure.story('GET     /files ')
    def test_files_get(self):

        assert self.response.status_code == 200
        # validate_json(self.json_dump, self.schema)
        # TestFiles.id = self.json_dump[0]['id']

    @pytest.allure.severity(pytest.allure.severity_level.NORMAL)
    @allure.feature('/files')
    @allure.story('GET     /files/:id')
    @pytest.mark.run(after='test_files_get')
    def test_files_get_id(self):
        pytest.skip('надо добавить файлы на сервер в инит базы')

        file_id = TestFiles.id
        response = self.sender.get('files/' + str(file_id))
        assert self.response.status_code == 200

        response_json = response.json()
        validate_json(response_json, self.schema_id)
