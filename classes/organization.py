__author__ = 'slugovskoy'
from classes.serializable import Serializable
from classes.role_access import RoleAccess
from helper.validator import *
from helper.generator import *


class OrganizationMark(Serializable):

    def __init__(self, id=''):
        self.id = id
        self.name = "organization test mark name " + random_word(12)
        self.shortName = "organization test mark shortName " + random_word(5)
        self.description = "organization test mark description " + random_word(33)


class Organization(Serializable):

    def __init__(self):
        self.name = "organization test  name " + random_word(12)
        self.telegraphName = "organization test telegraphName " + random_word(12)
        self.shortName = "organization test  shortName " + random_word(5)
        self.description = "organization test description " + random_word(33)
        self.country = "organization test conntry " + random_word(11)
        self.city = "organization test city " + random_word(11)
        self.postIndex = random_int(10000)
        self.representativeName = "organization test representativeName " + random_word(11)
        self.representativePosition = "organization test representativePosition " + random_word(11)
        self.representativePhone = random_int(100000)
        self.representativeEmail = "organization test representativeEmail " + random_word(11)
        self.contacts = []

