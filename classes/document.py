__author__ = 'slugovskoy'

class Document:

    def __init__(self, name='', numbered=False, project=1, document_type=1, description='', document_status=4, approved_person_name='', approved_date=None, approved_person_position='',id=None, files=[], approved_files=[], approved=False):
        self.id = id
        self.approved_date = approved_date
        self.name = name
        self.files = files
        self.approved_files = approved_files
        self.numbered = numbered
        self.project = project
        self.document_type = document_type
        self.description = description
        self.document_status = document_status
        self.approved_person_name = approved_person_name
        self.approved_person_position = approved_person_position
        self.body = dict()
        self.approved = approved

        self.body['id'] = self.id
        self.body['name'] = self.name
        self.body['numbered'] = self.numbered
        self.body['files'] = self.files
        self.body['approvedFiles'] = self.approved_files
        self.body['approvedDate'] = self.approved_date
        self.body['project'] = self.project
        self.body['documentType'] = self.document_type
        self.body['documentStatus'] = self.document_status
        self.body['approvedPerson'] = {"id": 1}
        self.body['name'] = self.name
        self.body['approved'] = self.approved

    def __str__(self):
        return str(self.body)


