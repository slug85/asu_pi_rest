__author__ = 'slugovskoy'
from enum import Enum

class States(Enum):
    normal = 'normal'
    acceptable = 'acceptable'
    undesirable = 'undesirable'
    critical = 'critical'