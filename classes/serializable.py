__author__ = 'slugovskoy'
import jsonpickle
import json
from helper.validator import *
from helper.generator import *

class Serializable:

    def json(self):
        json_dump = jsonpickle.encode(self, unpicklable=False)
        return json.loads(json_dump)
