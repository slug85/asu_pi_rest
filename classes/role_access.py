# coding=utf-8
__author__ = 'slugovskoy'
from classes.serializable import Serializable
from helper.validator import *
from helper.generator import *


class RoleAccess(Serializable):
    def __init__(self, typ='CREATE', access='PLAN_PI'):
        self.access = 'ARRANGEMENT_PLAN'
        self.accessReadable = 'Планы мероприятий'.decode('utf-8')
        self.type = typ
        self.value = access + '.' + typ

