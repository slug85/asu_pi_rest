# coding=utf-8
__author__ = 'slugovskoy'
from classes.serializable import Serializable
from classes.role_access import RoleAccess
from classes.project_type import ProjectType
from classes.subprogram import Subprogram
from classes.project_type import ProjectType
from classes.project_state import ProjectState
from classes.project_status import ProjectStatus
from classes.lifecycle_stage import LifeCycleStage
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest


class Project(Serializable):
    def __init__(self, user=None,
                 project_type=1,
                 project_status=1,
                 project_state=1,
                 project_lifecycle_stage=1,
                 sub_program=1, name=None,
                 project_tags=list()
                 ):
        rest = Rest()
        self.projectType = ProjectType(project_type)
        if name is None:
            self.name = "api test project full name " + random_word(random_int_in_range(5, 50))
        else:
            self.name = name
        self.shortName = "api test project sort name  " + random_word(random_int_in_range(5, 10))
        self.subprogram = Subprogram(id=sub_program)
        self.projectStatus = ProjectStatus(project_status)
        self.projectState = ProjectStatus(project_state)
        self.projectLifecycleStage = LifeCycleStage(project_lifecycle_stage)
        if user:
            self.projectCoordinator = user
        else:
            self.projectCoordinator = rest.get_users().json()[0]
        if project_tags:
            self.tags = [project_tags]
        self.statusReport = StatusReport()

class StatusReport:
    def __init__(self, period=None):
        if period is None:
            self.period = random_int_in_range(1, 4)

class ArrangementPlan(Serializable):
    def __init__(self, control_date=None, project_id=None, control_level=1):
        if control_date is not None:
            self.controlDate = control_date
            self.completionDate = control_date
        else:
            self.controlDate = get_unix_time_in_future(5)
            self.completionDate = get_unix_time_in_future(5)
        self.name = "test arrangement plan " + random_word(10)
        self.projectId = project_id
        self.controlLevel = control_level

class ArrangementPlanStage(Serializable):
    def __init__(self, end_date=None, arrangement_plan=None, project_id=None, number=1):
        if end_date is not None:
            self.endDate = end_date
            self.completionDate = end_date
        self.name = "test arrangement plan stage " + random_word(10)
        self.measureId = arrangement_plan
        self.projectId = project_id
        self.number = number

class Arrangement(Serializable):
    def __init__(self, end_date=None, arrangement_plan=None, arrangement_plan_stage=None, milestones_id=None, number=1):
        if end_date is not None:
            self.completionDate = end_date
        self.name = "test arrangement " + random_word(10)
        self.planId = arrangement_plan
        if arrangement_plan_stage is not None:
            self.arrangementPlanStage = arrangement_plan_stage
        if milestones_id is not None:
            self.milestones = milestones_id
        self.number = number

class PlanPi(Serializable):
    def __init__(self):
        self.name = 'test planpi ' + random_word(10)
        self.shortName = 'test planpi ' + random_word(3)
        self.description = 'test planpi ' + random_word(22)

class ProjectTag(Serializable):
    def __init__(self):
        self.label = 'test project tag ' + random_word(10)

class PersonAttribute(Serializable):
    def __init__(self):
        self.name = 'test person attribute ' + random_word(10)
        self.shortName = 'test person attribute ' + random_word(3)
        self.description = 'test person attribute ' + random_word(22)

class PlanPiWorkStatus(Serializable):
    def __init__(self):
        self.name = 'test planpi work status' + random_word(10)
        self.shortName = 'test planpi work status ' + random_word(3)
        self.description = 'test planpi work status ' + random_word(22)

class PlanPiWorkType(Serializable):
    def __init__(self, plan_pi_id=None, plan_pi_version=None, budget_line=None, responsible_person_id=None):
        self.name = 'test planpi ' + random_word(10)
        self.shortName = 'test planpi ' + random_word(3)
        self.description = 'test planpi ' + random_word(22)
        if plan_pi_id is None:
            self.planPi = {"id": 1}
            self.selectedPlanPi = {"id": 1}
        else:
            self.planPi = {"id": plan_pi_id}
            self.selectedPlanPi = {"id": plan_pi_id}
        if plan_pi_version is None:
            self.planPiVersion = {"planPi": {"id": 1}}
        if budget_line is not None:
            self.budgetLine = {"id": budget_line}
        if responsible_person_id is not None:
            self.responsiblePerson = {"id": responsible_person_id}

class Source(Serializable):
    def __init__(self):
        self.shortName = random_word(5) + 'test financial source'
        self.name = random_word(10) + 'test financial source'
        self.description = random_word(20)

class ExecutorSelection(Serializable):
    def __init__(self):
        self.shortName = random_word(5) + 'test executor selection'
        self.name = random_word(10) + 'test executor selection'
        self.description = random_word(20)

class BudgetLine(Serializable):
    def __init__(self, source=None):
        self.shortName = random_word(5) + 'test BudgetLine'
        self.name = random_word(10) + 'test BudgetLine'
        self.description = random_word(20)
        self.financialSource = source

class PlanPiWorkLastVersion(Serializable):
    def __init__(self, plan_pi, customer=1, executor=2, plan_pi_year=2016, executor_selection=None, cost=None,
                 financial_source_id=1, budget_line_id=1, work_type_id=1,
                 required_completion_date=None, previous_work_id=None, plan_pi_work_status=None, responsible_person=1
                 ):

        if executor_selection is None:
            self.executorSelection = 'Единственный исполнитель'.decode('utf-8')
        else:
            self.executorSelection = executor_selection.decode('utf-8')

        self.planPiVersion = {
            "planPi":
                {"id": plan_pi},
            "planYear": plan_pi_year
        }

        self.customer = {"id": customer}
        self.executor = {"id": executor}

        if cost is not None:
            self.planPiWorkTypeAttributes = {
                "workCostNoNds": cost
            }
        else:
            self.planPiWorkTypeAttributes = {
                "workCostNoNds": random_int(10000000)
            }

        self.primaryWorkType = {
            "id": work_type_id,
            "budgetLine": {
                "id": budget_line_id,
                "finincialSource": financial_source_id
            }
        }
        self.name = random_word(10) + 'test planpi work'

        if required_completion_date is not None:
            self.requiredCompletionDate = required_completion_date

        if previous_work_id is not None:
            self.previousPlanPiWork = {"id": previous_work_id}

        if plan_pi_work_status is None:
            self.planPiWorkStatus = {"id": 1}

        self.responsiblePerson = {"id": responsible_person}

class PlanPiWork(Serializable):
    def __init__(self):
        pass

class Control(Serializable):
    def __init__(self, person_id=None):
        self.controllerPosition = 'test controller position' + random_word(5)
        self.controllerName = 'test controller name' + random_word()
        self.level = random_int_in_range(1, 4)
        self.name = 'ЦЗ1'.decode('utf-8')
        self.shortName = 'test controller shortName' + random_word()
        if person_id is not None:
            self.person = {"id": person_id}


