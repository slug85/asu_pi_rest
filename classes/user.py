# coding=utf-8
__author__ = 'slugovskoy'
from helper.validator import *
from helper.generator import *
import json
from classes.serializable import Serializable
from classes.role import Role
from classes.organization import *

class Person(Serializable):

    def __init__(self):
        self.email = random_email()
        self.emailRegistration = False
        self.firstName = random_word(5)
        self.lastName = random_word(9)
        self.middleName = random_word(11)
        self.position = random_word(11)
        self.sendEmails = False
        self.user = User()
        self.documentTypeStatuses = [DocumentTypeStatuses()]

class User(Serializable):
    def __init__(self):
        self.newPassword = random_word(11)
        self.newPasswordRepeat = self.newPassword


class Control(Serializable):

    def __init__(self):
        self.controllerPosition = random_word(5) + 'test control position'
        self.controllerName = random_word(5).capitalize() + ' A.' + 'E.'
        self.name = 'Ц'.decode('utf-8')
        self.level = random_control_level()

class DocumentTypeStatuses(Serializable):
    def __init__(self, can_agree=False, can_approve=False, can_observe=False, document_type=1):
        self.canAgree = can_agree
        self.canApprove = can_approve
        self.canObserve = can_observe
        self.documentType = {"id": document_type}