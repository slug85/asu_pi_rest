__author__ = 'slugovskoy'
from classes.serializable import Serializable
from classes.statuses import States
from enum import Enum

class StateSetting(Serializable):

    def __init__(self, state=States.normal.value, threshold_value=95, color='#5cb85c'):

        self.state = state
        self.thresholdValue = threshold_value
        self.color = color

class StateSettingsType(Enum):
    milestone = 'milestone'
    arrangement_plan_stage = 'arrangementPlanStage'
    arrangement_plan = 'arrangementPlan'
    arrangement = 'arrangement'
    calendar_plan_stage = 'calendarPlanStage'
    project = 'project'


