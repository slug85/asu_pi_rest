# coding=utf-8
__author__ = 'slugovskoy'
from classes.serializable import Serializable
from classes.role_access import RoleAccess
from helper.validator import *
from helper.generator import *
from helper.api_methods import Rest

class Subprogram(Serializable):

    def __init__(self, actual=True, program_id=None, selected_program_id=None, id=None):
        self.number = random_int(9)
        self.description = 'Тестовая подпрограмма '.decode('utf-8') + random_word(12)
        self.name = 'Тестовая подпрограмма '.decode('utf-8') + random_word(7)
        self.shortName = 'Тестовая подпрограмма '.decode('utf-8') + random_word(3)
        self.actual = actual
        if program_id is None:
            self.program = {"id": 1}
        else:
            self.program = {"id": program_id}

        if selected_program_id is None:
            self.selectedProgram = {"id": 1}
        else:
            self.selectedProgram = {"id": selected_program_id}
        if id is not None:
            self.id = id

class Program(Serializable):

    def __init__(self, projects=None, id=None):
        if not projects:
            projects = []
        self.projects = projects
        self.name = 'Тестовая программа '.decode('utf-8') + random_word(12)
        self.shortName = 'Тестовая программа '.decode('utf-8') + random_word(5)
        self.description = 'Тестовая программа '.decode('utf-8') + random_word(33)
        if id is not None:
            self.id = id

