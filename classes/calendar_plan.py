__author__ = 'slugovskoy'
from classes.serializable import Serializable
from classes.role_access import RoleAccess
from helper.validator import *
from helper.generator import *


class CalendarPlanStage(Serializable):
    def __init__(self, number=1, start_date=None, end_date=None, confirm=False):
        self.name = 'test calendar plan stage ' + random_word(10)
        self.number = number
        if start_date is None:
            self.startDate = get_unix_time_in_past(5)
        else:
            self.startDate = start_date
        if end_date is None:
            self.endDate = get_unix_time_in_future(5)
        else:
            self.endDate = end_date
        self.confirm = confirm


class CalendarPlan(Serializable):
    def __init__(self, project_id=1, calendar_plan_stages=None, confirm=True):
        self.projectId = project_id
        if calendar_plan_stages is not None:
            self.calendarPlanStages = calendar_plan_stages
        self.confirm = confirm


class Milestone(Serializable):
    def __init__(self, project_id=1, planned_completion_date=None, calendar_plan_stage=None, confirm=False):
        self.project = project_id
        if planned_completion_date is None:
            self.plannedCompletionDate = get_unix_time()
        if calendar_plan_stage is not None:
            self.calendarPlanStage = calendar_plan_stage.id
        self.confirm = confirm
        self.name = " test milestone " + random_word(10)



