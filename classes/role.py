__author__ = 'slugovskoy'
from classes.serializable import Serializable
from classes.role_access import RoleAccess
from classes.role_access import *
from helper.validator import *
from helper.generator import *

class Role(Serializable):

    def __init__(self, roleAccesses=None):
        self.name = random_word(10)
        self.shortName = random_word(10)
        self.code = random_word(3)
        self.description = random_word(10)
        if roleAccesses is None:
            self.roleAccesses = [RoleAccess()]




