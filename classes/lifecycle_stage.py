__author__ = 'slugovskoy'
from classes.serializable import Serializable
from classes.role_access import RoleAccess
from helper.validator import *
from helper.generator import *

class LifeCycleStage(Serializable):

    def __init__(self, id=''):
        self.id = id
        self.name = random_word(10)
        self.shortName = random_word(10)
        self.number = random_int(55)



