# coding=utf-8
import collections
import pytest
import allure
import requests
from conf import setup
import json
import pprint
from session_keeper import PlaySessionKeeper
from helper.generator import *

class Sender:
    def __init__(self):
        self.url = setup.app_url
        self.pp = pprint.PrettyPrinter()
        self.user = setup.user
        self.password = setup.password

    @pytest.allure.step('POST {1}')
    def post(self, method, params=None, headers=None):
        full_url = self.url + method
        cookies = {'PLAY_SESSION': PlaySessionKeeper.play_session}
        content_type = {'Content-Type': 'application/json; charset=utf-8'}
        print '\n-------------------------------------POST-------------------------------------------'
        print('POST ' + full_url)
        print('params: ' + json.dumps(params))
        print('cookies: ' + json.dumps(cookies))
        print('headers: ' + json.dumps(headers))
        request = requests.post(full_url, data=json.dumps(params), headers=content_type, verify=False, cookies=cookies)

        try:
            if params:
                allure.attach('POST params', str(params))
            json_dump = json.dumps(request.json()).decode('unicode-escape')
            allure.attach('POST uri', full_url)
            allure.attach('POST response', json_dump)
            print json_dump
        except StandardError:
            allure.attach('POST uri', full_url)
            allure.attach('POST response', str(request.content))
            if params:
                allure.attach('POST params', str(params))
            print str(request.content)
        print '\n-------------------------------------POST DONE--------------------------------------'
        return request

    @pytest.allure.step('POST  upload')
    def post_upload(self, params=None):
        full_url = self.url + 'upload'
        cookies = {'PLAY_SESSION': PlaySessionKeeper.play_session}
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate',
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'multipart/form-data; boundary=---------------------------',
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Cache-Control': 'no-cache'
        }
        print '\n-------------------------------------POST UPLOAD-------------------------------------------'
        print('POST UPLOAD ' + full_url)
        print('file: ' + str(params))
        print('cookies: ' + str(cookies))
        print('headers: ' + str(headers))
        request = requests.post(full_url, data=params, headers=headers, verify=False, cookies=cookies)
        allure.attach('POST UPLOAD uri', full_url)
        allure.attach('POST UPLOAD response', str(request.content))
        allure.attach('POST UPLOAD params', str(params))
        allure.attach('POST UPLOAD HEADERS', str(headers))
        print str(request.content)
        print '\n-------------------------------------POST UPLOAD DONE--------------------------------------'
        return request

    @pytest.allure.step('GET {1}')
    def get(self, method):
        cookies = {'PLAY_SESSION': PlaySessionKeeper.play_session}
        full_url = self.url + method
        print '\n-------------------------------------GET-------------------------------------------'
        print('\nGET ' + full_url)
        print('cookies: ' + json.dumps(cookies))
        request = requests.get(full_url, verify=False, cookies=cookies)
        # assert request.status_code == 200
        try:
            json_dump = json.dumps(request.json()).decode('unicode-escape')
            allure.attach('GET uri', full_url)
            allure.attach('GET response', json_dump)
            print json_dump
        except StandardError:
            allure.attach('GET uri', full_url)
            allure.attach('GET response', str(request.content))
            print str(request.content)
        print '\n-------------------------------------GET DONE--------------------------------------'
        return request

    @pytest.allure.step('DELETE {1}')
    def delete(self, method):
        cookies = {'PLAY_SESSION': PlaySessionKeeper.play_session}
        full_url = self.url + method
        print '\n-------------------------------------DELETE-------------------------------------------'
        print('\nDELETE ' + full_url)
        print('cookies: ' + json.dumps(cookies))
        request = requests.delete(full_url, verify=False, cookies=cookies)
        # assert request.status_code == 200
        try:
            json_dump = json.dumps(request.json()).decode('unicode-escape')
            allure.attach('DELETE uri', full_url)
            allure.attach('DELETE response', json_dump)
            print json_dump
        except StandardError:
            allure.attach('DELETE uri', full_url)
            allure.attach('DELETE response', str(request.content))
            print str(request.content)
        print '\n-------------------------------------DELETE DONE--------------------------------------'
        return request

    @pytest.allure.step('PUT {1}')
    def put(self, method, params=None, headers=None):
        full_url = self.url + method
        cookies = {'PLAY_SESSION': PlaySessionKeeper.play_session}
        content_type = {'Content-Type': 'application/json; charset=utf-8'}
        print '\n-------------------------------------PUT-------------------------------------------'
        print('\nPUT ' + full_url)
        print('params: ' + json.dumps(params))
        print('cookies: ' + json.dumps(cookies))
        print('headers: ' + json.dumps(headers))
        request = requests.put(full_url, data=json.dumps(params), headers=content_type, verify=False, cookies=cookies)
        # assert request.status_code == 200

        try:
            if params:
                allure.attach('PUT params', str(params))
            json_dump = json.dumps(request.json()).decode('unicode-escape')
            allure.attach('PUT uri', full_url)
            allure.attach('PUT response', json_dump)
            print json_dump
        except StandardError:
            allure.attach('PUT uri', full_url)
            allure.attach('PUT response', str(request.content))
            if params:
                allure.attach('PUT params', str(params))
            print str(request.content)
        print '\n-------------------------------------PUT DONE--------------------------------------'
        return request

    @pytest.allure.step
    def logout(self):
        print '\n----------------------------------------------------------------------------------------'
        print '---------------------------------- LOGOUT ----------------------------------------------'
        print '----------------------------------------------------------------------------------------'
        response = self.post('logout')
        return response

    @pytest.allure.step
    def authorize(self):
        print '\n----------------------------------------------------------------------------------------'
        print '---------------------------------- AUTHORIZATION ---------------------------------------'
        print '----------------------------------------------------------------------------------------'
        print 'Play session: ' + str(PlaySessionKeeper().play_session)
        if PlaySessionKeeper.play_session is None:
            credentials = collections.OrderedDict()
            credentials['email'] = self.user
            credentials['password'] = self.password
            content_type = {'Content-Type': 'application/json; charset=utf-8'}
            response = self.post('login', credentials, headers=content_type)
            try:
                print 'login response: '
                jsondump = response.json().encode('utf8')
                pprint.pprint(jsondump)
                allure.attach('login response', jsondump)
            except StandardError:
                print 'Response in not in json format'
                print response.content

            try:
                session_cookie = response.cookies['PLAY_SESSION']
            except KeyError as e:
                raise KeyError('missing authorization cookie in response' + ' HEADERS:  ' + str(response.headers))

            print 'NEW PLAY_SESSION: ' + session_cookie
            print '\n----------------------------------------------------------------------------------------'
            print '---------------------------------- AUTHORIZATION DONE-----------------------------------'
            print '----------------------------------------------------------------------------------------'
            PlaySessionKeeper.play_session = session_cookie


            # # прописываю доступные типы документов для всех имеющихся статусов
            # documenttypes_items = self.get('documenttypes').json()
            # documenttypes = list()
            # for item in documenttypes_items:
            #     documenttypes.append(item['id'])
            #
            # data = dict()
            # data['documentTypes'] = documenttypes
            #
            # document_statuses_items = self.get('documentstatuses').json()
            # for i in document_statuses_items:
            #     self.put('documentstatuses/' + str(i['id']), data)

            return response
