__author__ = 'slugovskoy'
import pytest
from sender.request_sender import Sender

@pytest.fixture(scope="session", autouse=True)
def teardown(request):

    def logout():
        sender = Sender()
        sender.logout()
    request.addfinalizer(logout)


